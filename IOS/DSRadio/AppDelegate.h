//
//  AppDelegate.h
//  DSRadio
//
//  Created by Ariel Barragan on 1/22/14.
//
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
