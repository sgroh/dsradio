//
//  ViewController.m
//  DSRadio
//
//  Created by Ariel Barragan on 1/22/14.
//
//

#import "PlayerViewController.h"
#import <AVFoundation/AVFoundation.h>


@interface PlayerViewController ()
@property (nonatomic,strong) AVPlayer *player;
@property (strong, nonatomic) IBOutlet UIButton *playStopButton;
@end


@implementation PlayerViewController

NSString *const streamURL = @"http://streaming.radionomy.com/devspark";

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)playStopButtonClicked:(id)sender {
    // Check if player is stopped or playing
    if (self.player.rate == 0.0f) {
        self.player = [[AVPlayer alloc]initWithURL:[NSURL URLWithString:streamURL]];
        [self.player addObserver:self forKeyPath:@"status" options:0 context:nil];
        [self.playStopButton setTitle:@"STOP" forState:UIControlStateNormal];
    }
    else {
        [self.player pause];
        [self.player removeObserver:self forKeyPath:@"status"];
        self.player = nil;
        [self.playStopButton setTitle:@"PLAY" forState:UIControlStateNormal];
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if ([keyPath isEqualToString:@"status"]) {
        if (self.player.status == AVPlayerStatusReadyToPlay) {
            [self.player play];
        }
        else if (self.player.status == AVPlayerStatusFailed) {
        }
    }
}

@end
