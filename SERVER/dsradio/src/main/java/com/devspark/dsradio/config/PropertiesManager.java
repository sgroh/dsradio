package com.devspark.dsradio.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Properties for the service path routes
 *
 * @author Federico Lenzi - flenzi@devspark.com
 */
public class PropertiesManager {

    public static final String BASE_URI = "base.uri";
    public static final String CHANNEL_PATH = "channel.path";
    public static final String CHANNELS_PATH = "channels.path";
    public static final String CHANNELS_PLAYEDSONGS_PATH = "channels.playedSongs.path";
    public static final String CHANNELS_PLAYEDSONG_PATH = "channels.playedSong.path";
    public static final String CHANNELS_STREAM_PATH = "channels.stream.path";

    public static final String USERS_PATH = "users.path";
    public static final String USER_PATH = "user.path";
    public static final String USER_FAVOURITE_CHANNELS_PATH = "user.channels.favourites.path";
    public static final String USER_FAVOURITE_CHANNEL_PATH = "user.channels.favourite.path";

    public static final String SONG_PATH = "song.path";

    public static final String SONG_ID = "{songId}";
    public static final String CHANNEL_ID = "{channelId}";
    public static final String PLAYEDSONGS_ID = "{playedSongId}";
    public static final String USER_ID = "{userId}";

    private static Properties properties = new Properties();
    private String propFileName = "uriNames.properties";

    private static Logger logger = LoggerFactory.getLogger(PropertiesManager.class);

    private PropertiesManager() {
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(propFileName);
        try {
            properties.load(inputStream);
            logger.info("Property file {} loaded", propFileName);
        } catch (IOException e) {
            logger.error("Property file {} not found in the classpath\n {}", propFileName, e.toString());
        }
    }

    /**
     * Static initializer
     */
    static {
        new PropertiesManager();
    }

    public static String getProperty(String propertyName) {
        return properties.getProperty(propertyName);
    }

}