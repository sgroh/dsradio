package com.devspark.dsradio.controllers;

import com.mangofactory.swagger.annotations.ApiIgnore;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Federico Lenzi - flenzi@devspark.com
 */
@Controller
public class ApiDocumentationController {
    @ApiIgnore
    @RequestMapping(value = "/documentation", method = RequestMethod.GET)
    public String getDoc() {
        return "documentation.html";
    }
}
