package com.devspark.dsradio.controllers;

import com.devspark.dsradio.dto.ChannelDto;
import com.devspark.dsradio.dto.ChannelsDto;
import com.devspark.dsradio.dto.PlayedSongsDto;
import com.devspark.dsradio.dto.QueryParametersDto;
import com.devspark.dsradio.services.ChannelService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.inject.Inject;

/**
 * @author Federico Lenzi - flenzi@devspark.com
 */
@Controller
public class ChannelsController {

    private static final Logger logger = LoggerFactory.getLogger(ChannelsController.class);

    @Inject
    private ChannelService channelsService;

    /**
     * Gets the channel with the given ID
     * @param channelId
     * @param queryParameters
     * @return the channel
     */
    @RequestMapping(value = "/channels/{channelId}", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public
    @ResponseBody
    ChannelDto getChannelById(@PathVariable Long channelId, QueryParametersDto queryParameters) {
        return channelsService.getChannelById(channelId, queryParameters);
    }

    /**
     * Get all channels
     * @param queryParameters
     * @return the channels
     */
    @RequestMapping(value = "/channels", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public
    @ResponseBody
    ChannelsDto getChannels(QueryParametersDto queryParameters) {
        return new ChannelsDto(channelsService.getChannels(queryParameters));
    }

    /**
     * Creates a new channel
     * @param channelDTO
     * @return the channel
     */
    @RequestMapping(value = "/channels", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.CREATED)
    public
    @ResponseBody
    ChannelDto createChannel(@RequestBody ChannelDto channelDTO) {
        return channelsService.createChannel(channelDTO);
    }

    /**
     * Updates a channel
     * @param channelDTO
     * @return the channel
     */
    @RequestMapping(value = "/channels", method = RequestMethod.PUT)
    @ResponseStatus(value = HttpStatus.OK)
    public
    @ResponseBody
    void updateChannel(@RequestBody ChannelDto channelDTO) {
        channelsService.updateChannel(channelDTO);
    }

    /**
     * Deletes a channel
     * @param channelId
     */
    @RequestMapping(value = "/channels/{channelId}", method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public
    @ResponseBody
    void deleteChannel(@PathVariable Long channelId) {
        channelsService.deleteChannel(channelId);
    }



    @RequestMapping(value = "/channels/{channelId}/playedSongs", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public
    @ResponseBody
    PlayedSongsDto getPlayedSongs(@PathVariable Long channelId, QueryParametersDto queryParameters) {
        return channelsService.getPlayedSongs(channelId, queryParameters);
    }

}
