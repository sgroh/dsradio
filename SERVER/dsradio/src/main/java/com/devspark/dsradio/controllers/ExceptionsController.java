package com.devspark.dsradio.controllers;

import com.devspark.dsradio.exceptions.SongsServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.persistence.EntityNotFoundException;

/**
 * ExceptionHandler applied to all controllers
 *
 * @author Federico Lenzi - flenzi@devspark.com
 */
@ControllerAdvice
public class ExceptionsController extends ResponseEntityExceptionHandler {

    private static Logger logger = LoggerFactory.getLogger(ExceptionsController.class);

    /**
     * Handles the SongsServiceException See {@link com.devspark.dsradio.exceptions.SongsServiceException}.
     *
     * @param ex the exception
     */
    @ExceptionHandler(SongsServiceException.class)
    public ResponseEntity<Object> handleSongsServiceException(SongsServiceException ex) {
        logger.error(ex.toString());
        return new ResponseEntity<Object>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * Handles the EntityNotFoundException See <a href="http://docs.oracle.com/javaee/7/api/javax/persistence/EntityNotFoundException.html">EntityNotFoundException</a>
     *
     * @param ex the exception
     */
    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<Object> handleEntityNotFoundException(EntityNotFoundException ex) {
        logger.error(ex.toString());
        return new ResponseEntity<Object>(ex.getMessage(), HttpStatus.NOT_FOUND);
    }

    /**
     * Handles AccessDeniedException
     *
     * @param ex the exception
     */
    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<Object> handleRuntimeException(AccessDeniedException ex) {
        logger.error(ex.toString());
        return new ResponseEntity<Object>(HttpStatus.UNAUTHORIZED);
    }

    /**
     * Handles the remaining Runtime exceptions
     *
     * @param ex the exception
     */
    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<Object> handleRuntimeException(RuntimeException ex) {
        logger.error(ex.toString());
        return new ResponseEntity<Object>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
