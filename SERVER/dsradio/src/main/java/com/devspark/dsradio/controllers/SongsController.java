package com.devspark.dsradio.controllers;

import com.devspark.dsradio.exceptions.SongsServiceException;
import com.devspark.dsradio.services.SongService;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.inject.Inject;

/**
 * Songs controller.
 *
 * @author Federico Lenzi - flenzi@devspark.com
 */
@Controller
public class SongsController {

    private static final Logger logger = LoggerFactory.getLogger(SongsController.class);

    @Inject
    private SongService songService;

    /**
     * Handles likes to songs. See {@link com.devspark.dsradio.services.SongService#likeSong}.
     *
     * @param artist   artist of the song encoded in base64
     * @param songName name of the song encoded in base64
     * @return
     * @throws SongsServiceException
     */
    @RequestMapping(value = "/songs/{artist}/{songName}/likes", method = RequestMethod.PUT)
    @ResponseStatus(value = HttpStatus.OK)
    public void likeSong(@PathVariable final String artist, @PathVariable final String songName) throws SongsServiceException {
        String decodedArtist = StringUtils.newStringUtf8(Base64.decodeBase64(artist));
        String decodedSongName = StringUtils.newStringUtf8(Base64.decodeBase64(songName));

        songService.likeSong(decodedArtist, decodedSongName);
    }

    /**
     * Handles dislikes to songs. See {@link com.devspark.dsradio.services.SongService#dislikeSong}.
     *
     * @param artist   artist of the song encoded in base64
     * @param songName name of the song encoded in base64
     * @return
     * @throws SongsServiceException
     */
    @RequestMapping(value = "/songs/{artist}/{songName}/dislikes", method = RequestMethod.PUT)
    @ResponseStatus(value = HttpStatus.OK)
    public void dislikeSong(@PathVariable final String artist, @PathVariable final String songName) throws SongsServiceException {
        String decodedArtist = StringUtils.newStringUtf8(Base64.decodeBase64(artist));
        String decodedSongName = StringUtils.newStringUtf8(Base64.decodeBase64(songName));

        songService.dislikeSong(decodedArtist, decodedSongName);
    }

}
