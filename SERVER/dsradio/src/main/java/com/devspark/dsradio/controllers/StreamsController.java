package com.devspark.dsradio.controllers;

import com.devspark.dsradio.dto.StreamDto;
import com.devspark.dsradio.services.StreamService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.inject.Inject;

/**
 * @author Federico Lenzi - flenzi@devspark.com
 */
@Controller
public class StreamsController {
    private static final Logger logger = LoggerFactory.getLogger(StreamsController.class);

    @Inject
    private StreamService streamService;

    /**
     * Gets the channel's stream
     * @param channelId
     * @return the Strem
     */
    @RequestMapping(value = "/channels/{channelId}/stream", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public
    @ResponseBody
    StreamDto getChannelsStream(@PathVariable Long channelId) {
        return streamService.getStreamByChannelId(channelId);
    }

    /**
     * Creates the stream for a channel
     * @param channelId
     * @param streamDto
     * @return the Strem
     */
    @RequestMapping(value = "/channels/{channelId}/stream", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.CREATED)
    public
    @ResponseBody
    StreamDto createChannelsStream(@PathVariable Long channelId, @RequestBody StreamDto streamDto) {
        return streamService.createStream(channelId, streamDto);
    }
}
