package com.devspark.dsradio.controllers;

import com.devspark.dsradio.dto.ChannelDto;
import com.devspark.dsradio.dto.ChannelsFavoritesDto;
import com.devspark.dsradio.dto.UserDto;
import com.devspark.dsradio.entities.User;
import com.devspark.dsradio.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.inject.Inject;
import java.util.List;

/**
 * @author Federico Lenzi - flenzi@devspark.com
 */
@Controller
public class UsersController {

    private static final Logger logger = LoggerFactory.getLogger(UsersController.class);

    @Inject
    private UserService userService;

    /**
     * Gets the user information
     *
     * @param userId the user id
     * @return the user
     */
    @RequestMapping(value = "/users/{userId}", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    @PreAuthorize("#userId == principal.id")
    public
    @ResponseBody
    UserDto getUserById(@PathVariable Long userId) {
        return userService.getUserById(userId);
    }

    /**
     * Gets the authenticated user information
     *
     * @return the current user
     */
    @RequestMapping(value = "/user", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public
    @ResponseBody
    UserDto getAuthenticatedUser() {
        return getUserById(getCurrentAuthenticatedUser().getId());
    }

    /**
     * Gets the favourite channels for a user
     * @param userId the user id
     * @return the favourite channels
     */
    @RequestMapping(value = "/users/{userId}/favouriteChannels", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    @PreAuthorize("#userId == principal.id")
    public
    @ResponseBody
    ChannelsFavoritesDto getFavoriteChannels(@PathVariable Long userId) {
        List<ChannelDto> favoriteChannels = userService.getFavouriteChannels(userId);
        return getChannelsFavouritesDto(userId, favoriteChannels);
    }

    /**
     * Gets the favourite channels for the authenticated user
     * @return the favourite channels
     */
    @RequestMapping(value = "/user/favouriteChannels", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public
    @ResponseBody
    ChannelsFavoritesDto getFavoriteChannelsForAuthenticatedUser() {
        return getFavoriteChannels(getCurrentAuthenticatedUser().getId());
    }

    /**
     * Adds a channel as favourite
     *
     * @param userId the user id
     * @param channelDTO the channel
     * @return the favourite channels
     */
    @RequestMapping(value = "/users/{userId}/favouriteChannels", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    @PreAuthorize("#userId == principal.id")
    public
    @ResponseBody
    ChannelsFavoritesDto addFavouriteChannel(@PathVariable Long userId, @RequestBody ChannelDto channelDTO) {
        List<ChannelDto> favouriteChannels = userService.addFavouriteChannel(userId, channelDTO.getId());
        return getChannelsFavouritesDto(userId, favouriteChannels);
    }

    /**
     * Adds a channel as favourite for the authenticated user
     *
     * @param channelDTO the channel
     * @return the favourite channels
     */
    @RequestMapping(value = "/user/favouriteChannels", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public
    @ResponseBody
    ChannelsFavoritesDto addFavouriteChannelForAuthenticatedUser(@RequestBody ChannelDto channelDTO) {
        return addFavouriteChannel(getCurrentAuthenticatedUser().getId(), channelDTO);
    }

    /**
     * Deletes a channel from the favourites
     *
     * @param userId the user id
     * @param channelId the channel id
     * @return the favourite channels
     */
    @RequestMapping(value = "/users/{userId}/favouriteChannels/{channelId}", method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.OK)
    @PreAuthorize("#userId == principal.id")
    public
    @ResponseBody
    ChannelsFavoritesDto deleteFavouriteChannel(@PathVariable Long userId, @PathVariable Long channelId) {
        List<ChannelDto> favouriteChannels = userService.deleteFavouriteChannel(userId, channelId);
        return getChannelsFavouritesDto(userId, favouriteChannels);
    }

    /**
     * Deletes a channel from the favourites for the authenticated user
     *
     * @param channelId the channel id
     * @return the favourite channels
     */
    @RequestMapping(value = "/user/favouriteChannels/{channelId}", method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.OK)
    public
    @ResponseBody
    ChannelsFavoritesDto deleteFavouriteChannelForAuthenticatedUser(@PathVariable Long channelId) {
        return deleteFavouriteChannel(getCurrentAuthenticatedUser().getId(), channelId);
    }

    private User getCurrentAuthenticatedUser(){
        User user = userService.getAuthenticatedUser();
        if(user == null)
            throw new AccessDeniedException("User not authenticated");
        return user;
    }

    private ChannelsFavoritesDto getChannelsFavouritesDto(Long userId, List<ChannelDto> favouriteChannels) {
        ChannelsFavoritesDto channelsFavouritesDto = ChannelsFavoritesDto.newInstance(userId);
        channelsFavouritesDto.setItems(favouriteChannels);
        return channelsFavouritesDto;
    }

}
