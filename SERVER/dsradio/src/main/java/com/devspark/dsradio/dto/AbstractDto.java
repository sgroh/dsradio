package com.devspark.dsradio.dto;

/**
 *
 * Superclass for DTOs that the API use.
 * The href attribute represents the reference to the resource's location.
 *
 * @author Federico Lenzi - flenzi@devspark.com
 */
public abstract class AbstractDto {
    private String href;
    public abstract String getHref();
}
