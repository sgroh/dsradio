package com.devspark.dsradio.dto;

import com.devspark.dsradio.config.PropertiesManager;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

/**
 * @author Federico Lenzi - flenzi@devspark.com
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ChannelDto extends AbstractDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String name;
    private StreamDto stream; //api.com/1/stream
    private PlayedSongsDto playedSongs; //api.com/1/playedSongsDto
    private Boolean favourite;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public StreamDto getStream() {
        return stream;
    }

    public void setStream(StreamDto stream) {
        this.stream = stream;
    }

    public PlayedSongsDto getPlayedSongs() {
        return playedSongs;
    }

    public void setPlayedSongs(PlayedSongsDto playedSongs) {
        this.playedSongs = playedSongs;
    }

    public Boolean getFavourite() {
        return favourite;
    }

    public void setFavourite(Boolean favourite) {
        this.favourite = favourite;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ChannelDto that = (ChannelDto) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String getHref() {
        return (PropertiesManager.getProperty(PropertiesManager.BASE_URI) + PropertiesManager.getProperty(PropertiesManager.CHANNEL_PATH))
                .replace(PropertiesManager.CHANNEL_ID, this.getId().toString());
    }

    public ChannelDto() {
    }

}
