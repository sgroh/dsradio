package com.devspark.dsradio.dto;

import com.devspark.dsradio.config.PropertiesManager;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.List;

/**
 * Collection of channels.
 *
 * @author Federico Lenzi - flenzi@devspark.com
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ChannelsDto extends AbstractDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private List<ChannelDto> items;

    public ChannelsDto() {
        super();
    }

    public ChannelsDto(List<ChannelDto> channels) {
        super();
        this.items = channels;
    }

    public List<ChannelDto> getItems() {
        return items;
    }

    public void setItems(List<ChannelDto> items) {
        this.items = items;
    }

    @Override
    public String getHref() {
        return PropertiesManager.getProperty(PropertiesManager.BASE_URI) + PropertiesManager.getProperty(PropertiesManager.CHANNELS_PATH);
    }
}
