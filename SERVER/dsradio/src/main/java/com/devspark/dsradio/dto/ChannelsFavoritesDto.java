package com.devspark.dsradio.dto;

import com.devspark.dsradio.config.PropertiesManager;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Collection of user's favourite channels.
 * @author Federico Lenzi - flenzi@devspark.com
 */
public class ChannelsFavoritesDto extends ChannelsDto {

    private Long userId;

    @JsonIgnore
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public static ChannelsFavoritesDto newInstance(Long userlId) {
        ChannelsFavoritesDto channelsFavouritesDto = new ChannelsFavoritesDto();
        channelsFavouritesDto.setUserId(userlId);
        return channelsFavouritesDto;
    }

    @Override
    public String getHref() {
        return PropertiesManager.getProperty(PropertiesManager.BASE_URI) + PropertiesManager.getProperty(PropertiesManager.USER_FAVOURITE_CHANNELS_PATH)
                .replace(PropertiesManager.USER_ID, this.getUserId().toString());
    }
}
