package com.devspark.dsradio.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Federico Lenzi - flenzi@devspark.com
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PlayedSongDto extends SongDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy/MM/dd HH:mm:ss")
    private Date time;

    /**
     * Atrribute to build the href
     */
    @JsonIgnore
    private Long channelId;

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Long getChannelId() {
        return channelId;
    }

    public void setChannelId(Long channelId) {
        this.channelId = channelId;
    }

}
