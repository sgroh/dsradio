package com.devspark.dsradio.dto;

import com.devspark.dsradio.config.PropertiesManager;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.List;

/**
 * @author Federico Lenzi - flenzi@devspark.com
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PlayedSongsDto extends AbstractDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private List<PlayedSongDto> items;

    /**
     * Atrribute to build the href
     */
    @JsonIgnore
    private Long channelId;

    public List<PlayedSongDto> getItems() {
        return items;
    }

    public void setItems(List<PlayedSongDto> items) {
        this.items = items;
        for (PlayedSongDto item : items) {
            item.setChannelId(this.channelId);
        }
    }

    public Long getChannelId() {
        return channelId;
    }

    public void setChannelId(Long channelId) {
        this.channelId = channelId;
    }

    @Override
    public String getHref() {
        return (PropertiesManager.getProperty(PropertiesManager.BASE_URI) + PropertiesManager.getProperty(PropertiesManager.CHANNELS_PLAYEDSONGS_PATH))
                .replace(PropertiesManager.CHANNEL_ID, this.getChannelId().toString());
    }

    public static PlayedSongsDto newInstance(Long channelId) {
        PlayedSongsDto playedSongsDto = new PlayedSongsDto();
        playedSongsDto.setChannelId(channelId);
        return playedSongsDto;
    }
}
