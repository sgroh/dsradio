package com.devspark.dsradio.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents the query parameters that a call can have.
 *
 * @author Federico Lenzi - flenzi@devspark.com
 */
public class QueryParametersDto {
    public static final String STREAM = "stream";
    public static final String PLAYED_SONGS = "playedSongs";

    private List<String> expand = new ArrayList<>();
    private PageInfoDto pagination;

    public boolean isExpandable(String field) {
        return expand.contains(field);
    }

    public List<String> getExpand() {
        return expand;
    }

    public void setExpand(List<String> expand) {
        this.expand = expand;
    }

    public List<String> getExpandables() {
        return expand;
    }

    public void setExpandables(List<String> expand) {
        this.expand = expand;
    }

    public PageInfoDto getPagination() {
        return pagination;
    }

    public void setPaginations(PageInfoDto pagination) {
        this.pagination = pagination;
    }
}
