package com.devspark.dsradio.dto;

import com.devspark.dsradio.config.PropertiesManager;

import java.io.Serializable;

/**
 * @author Federico Lenzi - flenzi@devspark.com
 */
public class SongDto extends AbstractDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String author;
    private String name;
    private String genre;
    private Long duration;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    @Override
    public String getHref() {
        return (PropertiesManager.getProperty(PropertiesManager.BASE_URI) + PropertiesManager.getProperty(PropertiesManager.SONG_PATH))
                .replace(PropertiesManager.SONG_ID, this.getId().toString());
    }
}
