package com.devspark.dsradio.dto;

import com.devspark.dsradio.config.PropertiesManager;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

/**
 * @author Federico Lenzi - flenzi@devspark.com
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class StreamDto extends AbstractDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String url;
    private Integer bitRate;

    /**
     * Atrribute to build the href
     */
    @JsonIgnore
    private Long channelId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getBitRate() {
        return bitRate;
    }

    public void setBitRate(Integer bitRate) {
        this.bitRate = bitRate;
    }

    public Long getChannelId() {
        return channelId;
    }

    public void setChannelId(Long channelId) {
        this.channelId = channelId;
    }

    @Override
    public String getHref() {
        return (PropertiesManager.getProperty(PropertiesManager.BASE_URI) + PropertiesManager.getProperty(PropertiesManager.CHANNELS_STREAM_PATH))
                .replace(PropertiesManager.CHANNEL_ID, this.getChannelId().toString());
    }

    public static StreamDto newInstance(Long channelId) {
        StreamDto streamDto = new StreamDto();
        streamDto.setChannelId(channelId);
        return streamDto;
    }
}

