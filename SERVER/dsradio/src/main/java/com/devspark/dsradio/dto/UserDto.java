package com.devspark.dsradio.dto;

import com.devspark.dsradio.config.PropertiesManager;
import com.devspark.dsradio.entities.Role;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Set;

/**
 * @author Federico Lenzi - flenzi@devspark.com
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDto extends AbstractDto{
    private Long id;

    private String userName;

    private String email;

    private String token;

    private Set<Role> roles;

    private ChannelsDto favouriteChannels;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public ChannelsDto getFavouriteChannels() {
        return favouriteChannels;
    }

    public void setFavouriteChannels(ChannelsDto favouriteChannels) {
        this.favouriteChannels = favouriteChannels;
    }

    @Override
    public String getHref() {
        return (PropertiesManager.getProperty(PropertiesManager.BASE_URI) + PropertiesManager.getProperty(PropertiesManager.USER_PATH))
                .replace(PropertiesManager.USER_ID, this.getId().toString());
    }
}
