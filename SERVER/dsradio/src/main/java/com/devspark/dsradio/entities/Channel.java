package com.devspark.dsradio.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.io.Serializable;
import java.util.List;

/**
 * @author Federico Lenzi - flenzi@devspark.com
 */
@Entity
public class Channel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "channel_id")
    private Long id;

    private String name;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Stream stream;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "pk.channel", cascade = CascadeType.ALL)
    private List<ChannelSong> channelSongs;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Stream getStream() {
        return stream;
    }

    public void setStream(Stream stream) {
        this.stream = stream;
    }

    public List<ChannelSong> getChannelSongs() {
        return channelSongs;
    }

    public void setChannelSongs(List<ChannelSong> channelSongs) {
        this.channelSongs = channelSongs;
    }

    public Channel() {
    }

    public Channel(String name, Stream stream) {
        this.name = name;
        this.stream = stream;
    }

    public Channel(String name, Stream stream, List<ChannelSong> channelSongs) {
        this.name = name;
        this.stream = stream;
        this.channelSongs = channelSongs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Channel channel = (Channel) o;

        if (id != null ? !id.equals(channel.id) : channel.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
