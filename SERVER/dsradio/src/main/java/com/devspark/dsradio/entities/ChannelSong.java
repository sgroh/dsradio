package com.devspark.dsradio.entities;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Federico Lenzi - flenzi@devspark.com
 */
@Entity
@AssociationOverrides({@AssociationOverride(name = "pk.channel", joinColumns = @JoinColumn(name = "channel_id")),
        @AssociationOverride(name = "pk.song", joinColumns = @JoinColumn(name = "song_id"))})
public class ChannelSong implements Serializable {

    private ChannelSongId pk = new ChannelSongId();

    @Column(columnDefinition = "DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date time;

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    public ChannelSongId getPk() {
        return pk;
    }

    public void setPk(ChannelSongId pk) {
        this.pk = pk;
    }

    @Transient
    public Channel getChannel() {
        return getPk().getChannel();
    }

    public void setChannel(Channel channel) {
        getPk().setChannel(channel);
    }

    @Transient
    public Song getSong() {
        return getPk().getSong();
    }

    public void setSong(Song song) {
        getPk().setSong(song);
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        ChannelSong that = (ChannelSong) o;

        if (getPk() != null ? !getPk().equals(that.getPk()) : that.getPk() != null)
            return false;

        return true;
    }

    public int hashCode() {
        return (getPk() != null ? getPk().hashCode() : 0);
    }

}
