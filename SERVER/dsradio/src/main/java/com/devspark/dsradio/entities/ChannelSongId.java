package com.devspark.dsradio.entities;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * @author Federico Lenzi - flenzi@devspark.com
 */
@Embeddable
public class ChannelSongId implements Serializable {

    private Channel channel;

    private Song song;

    private static final long serialVersionUID = 1L;

    @ManyToOne
    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    @ManyToOne
    public Song getSong() {
        return song;
    }

    public void setSong(Song song) {
        this.song = song;
    }

    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        ChannelSongId that = (ChannelSongId) o;

        if (channel != null ? !channel.equals(that.channel) : that.channel != null)
            return false;
        if (song != null ? !song.equals(that.song) : that.song != null)
            return false;

        return true;
    }

    public int hashCode() {
        int result;
        result = (channel != null ? channel.hashCode() : 0);
        result = 31 * result + (song != null ? song.hashCode() : 0);
        return result;
    }
}
