package com.devspark.dsradio.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @author Federico Lenzi - flenzi@devspark.com
 */
@Entity
public class Song implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "song_id")
    private Long id;

    private String author;

    private String name;

    private String genre;

    private Long duration;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "pk.song", cascade = CascadeType.ALL)
    private Collection<UserSong> userLiked;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "pk.song", cascade = CascadeType.ALL)
    private List<ChannelSong> channelSongs;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Collection<UserSong> getUserLiked() {
        return userLiked;
    }

    public void setUserLiked(Collection<UserSong> userLiked) {
        this.userLiked = userLiked;
    }

    public List<ChannelSong> getChannelSongs() {
        return channelSongs;
    }

    public void setChannelSongs(List<ChannelSong> channelSongs) {
        this.channelSongs = channelSongs;
    }
}
