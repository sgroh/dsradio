package com.devspark.dsradio.entities;

import org.apache.commons.collections.SetUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Federico Lenzi - flenzi@devspark.com
 */
@Entity
public class User implements UserDetails, Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String userName;

    private String email;

    private String token;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(uniqueConstraints = @UniqueConstraint(columnNames = {"user_id", "role_id"}),
            joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
    private Set<Role> roles;


    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_channel", joinColumns = {
            @JoinColumn(name = "user_id", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "channel_id",
                    nullable = false, updatable = false) })
    private List<Channel> favouriteChannels;


    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private Collection<Suggestion> suggestions;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "pk.user", cascade = CascadeType.ALL)
    private Collection<UserSong> songsLikes;

    private static final long serialVersionUID = 1L;

    public User() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Collection<Suggestion> getSuggestions() {
        return suggestions;
    }

    public void setSuggestions(Collection<Suggestion> suggestions) {
        this.suggestions = suggestions;
    }

    public Collection<UserSong> getSongsLiked() {
        return songsLikes;
    }

    public void setSongsLiked(Collection<UserSong> songsLikes) {
        this.songsLikes = songsLikes;
    }

    public Set<Role> getRoles() {
        if (roles == null) {
            return null;
        }
        return SetUtils.unmodifiableSet(roles);
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public void replaceRoles(Collection<Role> rolesToAdd) {
        if (roles == null) {
            roles = new HashSet<Role>();
        }
        roles.retainAll(rolesToAdd);
        roles.addAll(rolesToAdd);
    }

    public List<Channel> getFavouriteChannels() {
        return favouriteChannels;
    }

    public void setFavouriteChannels(List<Channel> favouriteChannels) {
        this.favouriteChannels = favouriteChannels;
    }

    public void addFavouriteChannel(Channel channel){
        if(favouriteChannels == null){
            favouriteChannels = new ArrayList<>();
        }
        favouriteChannels.add(channel);
    }

    public User(Long id, String userName) {
        super();
        this.id = id;
        this.userName = userName;
    }

    public static User newUser(){
        User user = new User();
        return user;
    }

    //required by UserDetails
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roles;
    }

    @Override
    public String getPassword() {
        return token;
    }

    @Override
    public String getUsername() {
        return userName;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

}
