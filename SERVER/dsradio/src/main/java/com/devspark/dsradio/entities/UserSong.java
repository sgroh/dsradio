package com.devspark.dsradio.entities;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Transient;

/**
 * @author Federico Lenzi - flenzi@devspark.com
 */
@Entity
@AssociationOverrides({@AssociationOverride(name = "pk.user", joinColumns = @JoinColumn(name = "user_id")),
        @AssociationOverride(name = "pk.song", joinColumns = @JoinColumn(name = "song_id"))})
public class UserSong implements java.io.Serializable {

    private UserSongId pk = new UserSongId();

    @Column(name = "userLiked")
    private boolean userLiked;

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    public UserSongId getPk() {
        return pk;
    }

    public void setPk(UserSongId pk) {
        this.pk = pk;
    }

    @Transient
    public User getUser() {
        return getPk().getUser();
    }

    public void setUser(User user) {
        getPk().setUser(user);
    }

    @Transient
    public Song getSong() {
        return getPk().getSong();
    }

    public void setSong(Song song) {
        getPk().setSong(song);
    }

    public boolean isUserLiked() {
        return userLiked;
    }

    public void setUserLiked(boolean userLiked) {
        this.userLiked = userLiked;
    }

    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        UserSong that = (UserSong) o;

        if (getPk() != null ? !getPk().equals(that.getPk()) : that.getPk() != null)
            return false;

        return true;
    }

    public int hashCode() {
        return (getPk() != null ? getPk().hashCode() : 0);
    }

}
