package com.devspark.dsradio.entities;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * @author Federico Lenzi - flenzi@devspark.com
 */
@Embeddable
public class UserSongId implements Serializable {

    private User user;

    private Song song;

    private static final long serialVersionUID = 1L;

    @ManyToOne
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @ManyToOne
    public Song getSong() {
        return song;
    }

    public void setSong(Song song) {
        this.song = song;
    }

    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        UserSongId that = (UserSongId) o;

        if (user != null ? !user.equals(that.user) : that.user != null)
            return false;
        if (song != null ? !song.equals(that.song) : that.song != null)
            return false;

        return true;
    }

    public int hashCode() {
        int result;
        result = (user != null ? user.hashCode() : 0);
        result = 31 * result + (song != null ? song.hashCode() : 0);
        return result;
    }
}
