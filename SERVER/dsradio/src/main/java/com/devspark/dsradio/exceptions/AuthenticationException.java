package com.devspark.dsradio.exceptions;

/**
 * @author Federico Lenzi - flenzi@devspark.com
 */
public class AuthenticationException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public AuthenticationException(String message) {
        super(message);
    }

}