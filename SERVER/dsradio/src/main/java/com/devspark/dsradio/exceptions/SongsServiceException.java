package com.devspark.dsradio.exceptions;

/**
 * Exception thrown by the Song service.
 *
 * @author Federico Lenzi - flenzi@devspark.com
 */
public class SongsServiceException extends RuntimeException {

    private static final long serialVersionUID = 1L;

}