package com.devspark.dsradio.mappers;

import com.devspark.dsradio.dto.ChannelDto;
import com.devspark.dsradio.dto.PlayedSongDto;
import com.devspark.dsradio.dto.PlayedSongsDto;
import com.devspark.dsradio.dto.QueryParametersDto;
import com.devspark.dsradio.dto.StreamDto;
import com.devspark.dsradio.entities.Channel;
import com.devspark.dsradio.entities.ChannelSong;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Federico Lenzi - flenzi@devspark.com
 */
public class ChannelMapper {

    public static ChannelDto toDTO(Channel channel, ChannelDto channelDTO, QueryParametersDto queryParameters) {
        BeanUtils.copyProperties(channel, channelDTO, "channelSongs");

        //Copy Channel Songs
        List<ChannelSong> channelSongs = channel.getChannelSongs();


        //Copy Stream
        StreamDto streamDto = StreamDto.newInstance(channel.getId());
        if (queryParameters.isExpandable(QueryParametersDto.STREAM)) {
            if (channel.getStream() != null) {
                StreamMapper.toDTO(channel.getStream(), streamDto);
            }
        }
        channelDTO.setStream(streamDto);

        //Copy PlayedSongs
        PlayedSongsDto playedSongsDto = PlayedSongsDto.newInstance(channel.getId());
        if (queryParameters.isExpandable(QueryParametersDto.PLAYED_SONGS)) {
            if (channel.getChannelSongs() != null) {
                List<PlayedSongDto> items = new ArrayList<>();
                ChannelSongMapper.toDTO(channel.getChannelSongs(), items);
                playedSongsDto.setItems(items);
            }
        }
        channelDTO.setPlayedSongs(playedSongsDto);

        return channelDTO;
    }

    public static ChannelDto toDTO(Channel channel, ChannelDto channelDto) {
        return ChannelMapper.toDTO(channel, channelDto, new QueryParametersDto());
    }

    public static List<ChannelDto> toDTO(List<Channel> channels, List<ChannelDto> channelDTOs, QueryParametersDto queryParameters) {
        for (Channel channel : channels) {
            channelDTOs.add(toDTO(channel, new ChannelDto(), queryParameters));
        }
        return channelDTOs;
    }

    public static List<ChannelDto> toDTO(List<Channel> channels, List<ChannelDto> channelDtos) {
        return ChannelMapper.toDTO(channels, channelDtos, new QueryParametersDto());
    }

    public static Channel fromDTO(ChannelDto channelDTO, Channel channel) {
        BeanUtils.copyProperties(channelDTO, channel, "channelSongs", "id");

        PlayedSongsDto playedSongsDto = channelDTO.getPlayedSongs();
        if (playedSongsDto != null && playedSongsDto.getItems() != null) {
            channel.setChannelSongs(ChannelSongMapper.fromDTO(playedSongsDto.getItems(), new ArrayList<ChannelSong>()));
        }
        return channel;
    }

    public static List<Channel> fromDTO(List<ChannelDto> channelDTOs, List<Channel> channels) {
        for (ChannelDto channelDTO : channelDTOs) {
            channels.add(fromDTO(channelDTO, new Channel()));
        }
        return channels;
    }

}
