package com.devspark.dsradio.mappers;

import com.devspark.dsradio.dto.PlayedSongDto;
import com.devspark.dsradio.entities.ChannelSong;
import org.springframework.beans.BeanUtils;

import java.util.List;

/**
 * @author Federico Lenzi - flenzi@devspark.com
 */
public class ChannelSongMapper {
    public static PlayedSongDto toDTO(ChannelSong channelSong, PlayedSongDto playedSongDto) {
        BeanUtils.copyProperties(channelSong.getSong(), playedSongDto);
        playedSongDto.setTime(channelSong.getTime());
        return playedSongDto;
    }

    public static List<PlayedSongDto> toDTO(List<ChannelSong> channelSongs, List<PlayedSongDto> channelDTOs) {
        for (ChannelSong channel : channelSongs) {
            channelDTOs.add(toDTO(channel, new PlayedSongDto()));
        }
        return channelDTOs;
    }

    public static ChannelSong fromDTO(PlayedSongDto playedSongDto, ChannelSong channelSong) {
        BeanUtils.copyProperties(playedSongDto, channelSong, "channel", "id");
        return channelSong;
    }

    public static List<ChannelSong> fromDTO(List<PlayedSongDto> channelSongDTOs, List<ChannelSong> channelSongs) {
        for (PlayedSongDto playedSong : channelSongDTOs) {
            channelSongs.add(fromDTO(playedSong, new ChannelSong()));
        }
        return channelSongs;
    }

}
