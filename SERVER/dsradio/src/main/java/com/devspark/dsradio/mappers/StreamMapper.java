package com.devspark.dsradio.mappers;

import com.devspark.dsradio.dto.StreamDto;
import com.devspark.dsradio.entities.Stream;
import org.springframework.beans.BeanUtils;

/**
 * @author Federico Lenzi - flenzi@devspark.com
 */
public class StreamMapper {

    public static StreamDto toDTO(Stream stream, StreamDto streamDto) {
        BeanUtils.copyProperties(stream, streamDto);
        return streamDto;
    }

    public static Stream fromDTO(StreamDto streamDto, Stream stream) {
        BeanUtils.copyProperties(streamDto, stream);
        return stream;
    }
}