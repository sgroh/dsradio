package com.devspark.dsradio.mappers;

import com.devspark.dsradio.dto.ChannelsFavoritesDto;
import com.devspark.dsradio.dto.UserDto;
import com.devspark.dsradio.entities.User;
import org.springframework.beans.BeanUtils;

/**
 * @author Federico Lenzi - flenzi@devspark.com
 */
public class UserMapper {

    public static UserDto toDto(User user, UserDto userDto){
        //Token must not be returned
        BeanUtils.copyProperties(user, userDto, "token", "roles");
        userDto.setFavouriteChannels(ChannelsFavoritesDto.newInstance(user.getId()));

        return userDto;
    }

    public static User fromDto(UserDto userDto, User user){
        BeanUtils.copyProperties(userDto, user, "id");
        return user;
    }
}
