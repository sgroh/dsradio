package com.devspark.dsradio.repositories;

import com.devspark.dsradio.entities.ChannelSong;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author Federico Lenzi - flenzi@devspark.com
 */
public interface ChannelSongsRepository extends JpaRepository<ChannelSong, Long> {

    @Query("select cs from ChannelSong as cs where cs.pk.channel.id = ?1")
    List<ChannelSong> findAllByChannelId(Long id);

    @Query("select cs from ChannelSong as cs where cs.pk.song.id = ?1")
    List<ChannelSong> findAllBySongId(Long id);

}
