package com.devspark.dsradio.repositories;

import com.devspark.dsradio.entities.Channel;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Federico Lenzi - flenzi@devspark.com
 */
public interface ChannelsRepository extends JpaRepository<Channel, Long> {

}
