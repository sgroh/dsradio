package com.devspark.dsradio.repositories;

import com.devspark.dsradio.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Federico Lenzi - flenzi@devspark.com
 */
public interface RolesRepository extends JpaRepository<Role, Long> {
    Role findByName(String name);
}
