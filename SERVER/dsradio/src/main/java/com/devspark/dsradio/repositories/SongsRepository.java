package com.devspark.dsradio.repositories;

import com.devspark.dsradio.entities.Song;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Federico Lenzi - flenzi@devspark.com
 */
public interface SongsRepository extends JpaRepository<Song, Long> {

}
