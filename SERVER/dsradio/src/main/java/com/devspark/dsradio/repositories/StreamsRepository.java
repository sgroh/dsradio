package com.devspark.dsradio.repositories;

import com.devspark.dsradio.entities.Stream;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Federico Lenzi - flenzi@devspark.com
 */
public interface StreamsRepository extends JpaRepository<Stream, Long> {

}
