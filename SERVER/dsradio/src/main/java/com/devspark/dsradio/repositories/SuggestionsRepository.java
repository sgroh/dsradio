package com.devspark.dsradio.repositories;

import com.devspark.dsradio.entities.Suggestion;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Federico Lenzi - flenzi@devspark.com
 */
public interface SuggestionsRepository extends JpaRepository<Suggestion, Long> {

}
