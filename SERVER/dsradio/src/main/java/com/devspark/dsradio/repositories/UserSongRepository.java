package com.devspark.dsradio.repositories;

import com.devspark.dsradio.entities.UserSong;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author Federico Lenzi - flenzi@devspark.com
 */
public interface UserSongRepository extends JpaRepository<UserSong, Long> {

    @Query(value = "select * from user_song u where u.user_id = ?1 and u.user_liked = 1", nativeQuery = true)
    List<UserSong> findUserLikes(Long id);

}
