package com.devspark.dsradio.repositories;

import com.devspark.dsradio.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author Federico Lenzi - flenzi@devspark.com
 */
public interface UsersRepository extends JpaRepository<User, Long> {

    @Query("select u from User as u join fetch u.suggestions where u.id = ?1")
    User findByIdWithSuggestions(Long id);

    @Query("select u from User as u join fetch u.songsLikes")
    List<User> findAllWithLikes();

    @Query("select u from User as u join fetch u.songsLikes where u.id = ?1")
    User findByIdWithLikes(Long id);

    User findByToken(String token);

    User findByEmail(String email);
}
