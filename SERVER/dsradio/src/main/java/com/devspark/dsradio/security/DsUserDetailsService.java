package com.devspark.dsradio.security;

import com.devspark.dsradio.dto.UserDto;
import com.devspark.dsradio.entities.Role;
import com.devspark.dsradio.entities.User;
import com.devspark.dsradio.exceptions.AuthenticationException;
import com.devspark.dsradio.mappers.UserMapper;
import com.devspark.dsradio.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

/**
 * @author Federico Lenzi - flenzi@devspark.com
 */
@Service
public class DsUserDetailsService implements UserDetailsService {

    private static final Logger logger = LoggerFactory.getLogger(DsUserDetailsService.class);

    @Inject
    private UserService userService;

    @Inject
    private UserAuthenticator userAuthenticator;

    @Override
    public User loadUserByUsername(final String token) throws AuthenticationException {
        User user = userService.getUserByToken(token);
        if(user != null){
            return user;
        }
        else{
            try {
                GoogleUserInfo googleUserInfo = userAuthenticator.authenticateUser(token);

                //Creates the user if it doesn't exist, otherwise updates token
                User retrievedUser = userService.getUserByEmail(googleUserInfo.getEmail());
                if(retrievedUser == null){
                    UserDto newUser = new UserDto();
                    newUser.setEmail(googleUserInfo.getEmail());
                    newUser.setUserName(googleUserInfo.getName());
                    newUser.setToken(token);
                    newUser = userService.createUser(newUser, Role.ROLE_USER);

                    user = UserMapper.fromDto(newUser, User.newUser());
                    user.setId(newUser.getId());
                    user.setRoles(newUser.getRoles());
                    return user;
                }
                else{
                    retrievedUser.setToken(token);
                    userService.updateUserToken(retrievedUser);
                    return retrievedUser;
                }
            } catch (Exception e) {
                logger.error("Authentication error {}", e.toString());
                throw new BadCredentialsException("Invalid token");
            }
        }
    }

}
