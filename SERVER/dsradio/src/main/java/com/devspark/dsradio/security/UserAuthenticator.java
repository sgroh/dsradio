package com.devspark.dsradio.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.httpclient.protocol.ProtocolSocketFactory;
import org.apache.commons.httpclient.protocol.SSLProtocolSocketFactory;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.jboss.resteasy.client.ClientExecutor;
import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;
import org.jboss.resteasy.client.core.executors.ApacheHttpClient4Executor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * @author Federico Lenzi - flenzi@devspark.com
 */
@Service
public class UserAuthenticator {

    private static final Logger logger = LoggerFactory.getLogger(UserAuthenticator.class);
    private String host = "https://www.googleapis.com/oauth2/v2/userinfo?part=id&mine=true&access_token=";
    private ObjectMapper mapper = new ObjectMapper();

    protected HttpClient getDefaultHttpClient() {
        ProtocolSocketFactory protocolSocketFactory = new SSLProtocolSocketFactory();
        Protocol easyhttps = new Protocol("https", protocolSocketFactory, 443);
        Protocol.registerProtocol("https", easyhttps);
        HttpClient httpClient = new DefaultHttpClient();
        return httpClient;
    }

    public GoogleUserInfo authenticateUser(String token) throws Exception {
        try {
            HttpClient httpClient = getDefaultHttpClient();
            ClientExecutor clientExecutor = new ApacheHttpClient4Executor(httpClient);
            ClientRequest req = new ClientRequest(host + token, clientExecutor);
            req.setHttpMethod("GET");
            ClientResponse<String> res;
            res = req.get(String.class);
            String result = res.getEntity();
            GoogleUserInfo googleUserInfo = mapper.readValue(result, GoogleUserInfo.class);
            return googleUserInfo;
        } catch (Exception e) {
            logger.error("Invalid Token {} ", e.toString());
            throw new Exception(e.getMessage());
        }
    }
}
