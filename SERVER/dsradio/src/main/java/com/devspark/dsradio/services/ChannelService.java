package com.devspark.dsradio.services;


import com.devspark.dsradio.dto.ChannelDto;
import com.devspark.dsradio.dto.PlayedSongsDto;
import com.devspark.dsradio.dto.QueryParametersDto;

import java.util.List;

/**
 * @author Federico Lenzi - flenzi@devspark.com
 */
public interface ChannelService {

    ChannelDto getChannelById(Long channelId);

    ChannelDto getChannelById(Long channelId, QueryParametersDto queryParameters);

    List<ChannelDto> getChannels();

    List<ChannelDto> getChannels(QueryParametersDto queryParameters);

    ChannelDto createChannel(ChannelDto channelDTO);

    void updateChannel(ChannelDto channelDTO);

    void deleteChannel(Long channelId);

    PlayedSongsDto getPlayedSongs(Long channelId, QueryParametersDto queryParameters);
}
