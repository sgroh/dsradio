package com.devspark.dsradio.services;

import com.devspark.dsradio.dto.ChannelDto;
import com.devspark.dsradio.dto.PlayedSongDto;
import com.devspark.dsradio.dto.PlayedSongsDto;
import com.devspark.dsradio.dto.QueryParametersDto;
import com.devspark.dsradio.entities.Channel;
import com.devspark.dsradio.entities.User;
import com.devspark.dsradio.mappers.ChannelMapper;
import com.devspark.dsradio.mappers.ChannelSongMapper;
import com.devspark.dsradio.repositories.ChannelSongsRepository;
import com.devspark.dsradio.repositories.ChannelsRepository;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.inject.Inject;
import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Federico Lenzi - flenzi@devspark.com
 */
@Service
public class ChannelServiceImpl implements ChannelService {
    @Inject
    private ChannelsRepository channelsRepository;

    @Inject
    private ChannelSongsRepository channelSongsRepository;

    @Inject
    private UserService userService;

    private static final Logger logger = LoggerFactory.getLogger(ChannelServiceImpl.class);

    protected ChannelServiceImpl() {
    }

    protected ChannelServiceImpl(ChannelsRepository channelsRepository,
                                 ChannelSongsRepository channelSongsRepository, UserService userService) {
        this.channelsRepository = channelsRepository;
        this.channelSongsRepository = channelSongsRepository;
        this.userService = userService;
    }

    @Override
    @Transactional(readOnly = true)
    public ChannelDto getChannelById(Long channelId, QueryParametersDto queryParameters) throws EntityNotFoundException {
        Channel channel = channelsRepository.findOne(channelId);
        if (channel == null) {
            throw new EntityNotFoundException("Channel not found");
        }
        //Fetch ChannelSongs
        if (queryParameters.isExpandable(QueryParametersDto.PLAYED_SONGS)) {
            channel.setChannelSongs(channelSongsRepository.findAllByChannelId(channel.getId()));
        }
        return ChannelMapper.toDTO(channel, new ChannelDto(), queryParameters);
    }

    @Override
    public ChannelDto getChannelById(Long channelId) {
        return getChannelById(channelId, new QueryParametersDto());
    }

    @Override
    @Transactional(readOnly = true)
    public List<ChannelDto> getChannels(QueryParametersDto queryParameters){
        List<Channel> channels = channelsRepository.findAll();
        if (CollectionUtils.isEmpty(channels)) {
            return new ArrayList<>();
        }
        //Fetch ChannelSongs
        if (queryParameters.isExpandable(QueryParametersDto.PLAYED_SONGS)) {
            for(Channel channel : channels){
                channel.setChannelSongs(channelSongsRepository.findAllByChannelId(channel.getId()));
            }
        }

        List<ChannelDto> channelsDto = ChannelMapper.toDTO(channels, new ArrayList<ChannelDto>(), queryParameters);

        //Sets the favourite field for a user
        User loggedUser = userService.getAuthenticatedUser();
        if (!CollectionUtils.isEmpty(channels) && loggedUser != null) {
            for (ChannelDto channelDto : channelsDto) {
                channelDto.setFavourite(false);
            }
            List<ChannelDto> favouriteChannelDtos =  userService.getFavouriteChannels(loggedUser.getId());
            for (ChannelDto favouriteChannelDto : favouriteChannelDtos) {
                if(channelsDto.contains(favouriteChannelDto))
                    channelsDto.get(channelsDto.indexOf(favouriteChannelDto)).setFavourite(true);
            }
        }
        return channelsDto;
    }

    @Override
    public List<ChannelDto> getChannels() {
        return getChannels(new QueryParametersDto());
    }

    @Override
    @Transactional
    public ChannelDto createChannel(ChannelDto channelDTO) {
        Channel channel = new Channel();
        ChannelMapper.fromDTO(channelDTO, channel);
        channel = channelsRepository.save(channel);
        return ChannelMapper.toDTO(channel, new ChannelDto());
    }

    @Override
    public void updateChannel(ChannelDto channelDTO) {
        Validate.notNull(channelDTO.getId(), "Id can't be null when updating");

        Channel channel = channelsRepository.findOne(channelDTO.getId());
        ChannelMapper.fromDTO(channelDTO, channel);
        channelsRepository.save(channel);
    }

    @Override
    public void deleteChannel(Long channelId) {
        Validate.notNull(channelId, "Id can't be null when deleting");

        Channel channel = channelsRepository.findOne(channelId);
        if (channel == null) {
            throw new EntityNotFoundException("Channel not found");
        }
        channelsRepository.delete(channel);
    }

    @Override
    public PlayedSongsDto getPlayedSongs(Long channelId, QueryParametersDto queryParameters) {
        PlayedSongsDto playedSongsDto = PlayedSongsDto.newInstance(channelId);
        playedSongsDto.setItems(ChannelSongMapper.toDTO(channelSongsRepository.findAllByChannelId(channelId), new ArrayList<PlayedSongDto>()));
        return playedSongsDto;
    }

}
