package com.devspark.dsradio.services;

/**
 * Services related to songs.
 *
 * @author Federico Lenzi - flenzi@devspark.com
 */
public interface SongService {

    String likeSong(String artist, String songName);

    String dislikeSong(String artist, String songName);

}
