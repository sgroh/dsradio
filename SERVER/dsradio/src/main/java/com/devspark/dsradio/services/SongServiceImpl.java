package com.devspark.dsradio.services;

import com.devspark.dsradio.exceptions.SongsServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Services related to songs.
 *
 * @author Federico Lenzi - flenzi@devspark.com
 */
@Service
public class SongServiceImpl implements SongService {
    private static Logger logger = LoggerFactory.getLogger(SongServiceImpl.class);

    /**
     * Like song.
     *
     * @param artist   name of the artist
     * @param songName name of the song
     * @return
     */
    public String likeSong(final String artist, final String songName) throws SongsServiceException {
        logger.debug("Like {} - {}", artist, songName);

        return "Not implemented yet";
    }

    /**
     * Dislike song.
     *
     * @param artist   name of the artist
     * @param songName name of the song
     * @return
     */
    public String dislikeSong(final String artist, final String songName) throws SongsServiceException {
        logger.debug("Dislike {} - {}", artist, songName);

        return "Not implemented yet";
    }

}
