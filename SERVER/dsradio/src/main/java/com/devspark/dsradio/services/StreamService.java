package com.devspark.dsradio.services;

import com.devspark.dsradio.dto.StreamDto;

/**
 * @author Federico Lenzi - flenzi@devspark.com
 */
public interface StreamService {

    StreamDto getStreamByChannelId(Long channelId);

    StreamDto createStream(Long channelId, StreamDto streamDto);
}
