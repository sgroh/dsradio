package com.devspark.dsradio.services;

import com.devspark.dsradio.dto.StreamDto;
import com.devspark.dsradio.entities.Channel;
import com.devspark.dsradio.entities.Stream;
import com.devspark.dsradio.mappers.StreamMapper;
import com.devspark.dsradio.repositories.ChannelsRepository;
import com.devspark.dsradio.repositories.StreamsRepository;
import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityNotFoundException;

/**
 * @author Federico Lenzi - flenzi@devspark.com
 */
@Service
public class StreamServiceImpl implements StreamService {

    @Inject
    private ChannelsRepository channelsRepository;

    @Inject
    private StreamsRepository streamsRepository;

    @Override
    @Transactional(readOnly = true)
    public StreamDto getStreamByChannelId(Long channelId) {
        Channel channel = channelsRepository.findOne(channelId);
        if (channel == null) {
            throw new EntityNotFoundException("Channel not found");
        }
        if (channel.getStream() == null) {
            throw new EntityNotFoundException("Stream not found");
        }

        return StreamMapper.toDTO(channel.getStream(), StreamDto.newInstance(channel.getId()));
    }

    @Override
    @Transactional
    public StreamDto createStream(Long channelId, StreamDto streamDto) {
        Validate.isTrue(streamDto.getId() == null, "Stream Id must be null");

        Channel channel = channelsRepository.findOne(channelId);
        if (channel == null) {
            throw new EntityNotFoundException("Channel not found");
        }
        //Creates new Stream
        Stream stream = StreamMapper.fromDTO(streamDto, new Stream());
        stream = streamsRepository.save(stream);
        //Saves old Stream to delete
        Stream streamToDelete = channel.getStream();
        channel.setStream(stream);
        channelsRepository.save(channel);

        //Delete previous Stream
        if (streamToDelete != null) {
            streamsRepository.delete(streamToDelete);
        }

        return StreamMapper.toDTO(channel.getStream(), StreamDto.newInstance(channel.getId()));

    }
}
