package com.devspark.dsradio.services;

import com.devspark.dsradio.dto.ChannelDto;
import com.devspark.dsradio.dto.UserDto;
import com.devspark.dsradio.entities.User;

import java.util.List;

/**
 * @author Federico Lenzi - flenzi@devspark.com
 */
public interface UserService {
    User getUserByToken(String token);

    UserDto createUser(UserDto userDto);

    UserDto createUser(UserDto newUser, String role_user);

    User getUserByEmail(String email);

    User updateUserToken(User user);

    UserDto getUserById(Long userId);

    List<ChannelDto> getFavouriteChannels(Long userId);

    List<ChannelDto> addFavouriteChannel(Long userId, Long channelId);

    List<ChannelDto> deleteFavouriteChannel(Long userId, Long channelId);

    User getAuthenticatedUser();
}
