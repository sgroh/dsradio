package com.devspark.dsradio.services;

import com.devspark.dsradio.dto.ChannelDto;
import com.devspark.dsradio.dto.UserDto;
import com.devspark.dsradio.entities.Channel;
import com.devspark.dsradio.entities.Role;
import com.devspark.dsradio.entities.User;
import com.devspark.dsradio.mappers.ChannelMapper;
import com.devspark.dsradio.mappers.UserMapper;
import com.devspark.dsradio.repositories.ChannelsRepository;
import com.devspark.dsradio.repositories.RolesRepository;
import com.devspark.dsradio.repositories.UsersRepository;
import org.apache.commons.lang3.Validate;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Federico Lenzi - flenzi@devspark.com
 */
@Service
public class UserServiceImpl implements UserService {

    @Inject
    private UsersRepository usersRepository;

    @Inject
    private ChannelsRepository channelsRepository;

    @Inject
    private RolesRepository rolesRepository;

    @Override
    @Transactional(readOnly = true)
    public User getUserByToken(String token) {
        Validate.notNull(token);
        return usersRepository.findByToken(token);
    }

    @Override
    @Transactional
    public UserDto createUser(UserDto userDto) {
        User user = new User();
        UserMapper.fromDto(userDto, user);
        user = usersRepository.save(user);
        return UserMapper.toDto(user, new UserDto());
    }

    @Override
    public UserDto createUser(UserDto newUser, String role_user) {
        Set<Role> roles = new HashSet<>();
        roles.add(rolesRepository.findByName(Role.ROLE_USER));
        newUser.setRoles(roles);
        return this.createUser(newUser);
    }

    @Override
    @Transactional(readOnly = true)
    public User getUserByEmail(String email) {
        Validate.notNull(email);
        User user = usersRepository.findByEmail(email);
        if (user == null) {
            return null;
        }
        return user;
    }

    @Override
    public User updateUserToken(User user) {
        Validate.notNull(user.getId());
        User retrievedUser = usersRepository.findOne(user.getId());
        retrievedUser.setToken(user.getToken());
        retrievedUser = usersRepository.save(retrievedUser);
        return retrievedUser;
    }

    @Override
    public UserDto getUserById(Long userId) {
        User user = usersRepository.findOne(userId);
        if (user == null) {
            throw new EntityNotFoundException("User not found");
        }
        return UserMapper.toDto(user, new UserDto());
    }

    @Override
    @Transactional(readOnly = true)
    public List<ChannelDto> getFavouriteChannels(Long userId) {
        User user = usersRepository.findOne(userId);
        if (user == null) {
            throw new EntityNotFoundException("User not found");
        }
        return ChannelMapper.toDTO(user.getFavouriteChannels(), new ArrayList<ChannelDto>());
    }

    @Override
    @Transactional
    public List<ChannelDto> addFavouriteChannel(Long userId, Long channelId) {
        User user = usersRepository.findOne(userId);
        if (user == null) {
            throw new EntityNotFoundException("User not found");
        }
        Channel channel = channelsRepository.findOne(channelId);
        if (channel == null) {
            throw new EntityNotFoundException("Channel not found");
        }
        user.addFavouriteChannel(channel);
        user = usersRepository.save(user);
        return ChannelMapper.toDTO(user.getFavouriteChannels(), new ArrayList<ChannelDto>());
    }

    @Override
    @Transactional
    public List<ChannelDto> deleteFavouriteChannel(Long userId, Long channelId) {
        User user = usersRepository.findOne(userId);
        if (user == null) {
            throw new EntityNotFoundException("User not found");
        }
        Channel channel = channelsRepository.findOne(channelId);
        if (channel == null) {
            throw new EntityNotFoundException("Channel not found");
        }
        List<Channel> favouriteChannels = user.getFavouriteChannels();
        favouriteChannels.remove(channel);
        user = usersRepository.save(user);
        return ChannelMapper.toDTO(user.getFavouriteChannels(), new ArrayList<ChannelDto>());
    }

    @Override
    public User getAuthenticatedUser() {
       try{
           return  (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
       }catch (Exception e){
           return null;
       }
    }

}
