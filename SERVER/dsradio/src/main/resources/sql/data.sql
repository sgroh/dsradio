INSERT INTO stream (id, bit_rate, url) VALUES (1, 96, 'http://listen.radionomy.com/devspark');
INSERT INTO stream (id, bit_rate, url) VALUES (2, 128, 'http://pub1.sky.fm/sky_tophits');
INSERT INTO stream (id, bit_rate, url) VALUES (3, 128, 'http://pub1.sky.fm/sky_bossanova');
INSERT INTO stream (id, bit_rate, url) VALUES (10, 128, 'http://pub1.sky.fm/sky_poprock');
INSERT INTO stream (id, bit_rate, url) VALUES (11, 128, 'http://pub1.sky.fm/sky_ska');

INSERT INTO channel (channel_id, name, stream) VALUES (1, 'PopRock', 10);
INSERT INTO channel (channel_id, name, stream) VALUES (2, 'Devspark', 1);
INSERT INTO channel (channel_id, name, stream) VALUES (3, 'Top hits', 2);
INSERT INTO channel (channel_id, name, stream) VALUES (4, 'Ska', 11);

INSERT INTO role (id, name) VALUES (1, 'ROLE_USER');
INSERT INTO role (id, name) VALUES (2, 'ROLE_ADMIN');

INSERT INTO user (id, email, token, user_name) VALUES
  (5, 'federicolenzi@gmail.com', 'ya29.1.AADtN_Wkt8LpuhmOY0vGKLbNVdIAR_onp84Ir-d-oWaB60Tg5OQaOO3IIRv2QrIO',
   'Federico Lenzi');
INSERT INTO user (id, email, token, user_name) VALUES  (6, 'gchiacchio@devspark.com', 'ya29.1.AADtN_Uda4CMmC6fzErqVyUDUUAFjWiOODJC86G178yEV9iLecEmsxKuz5Z0yQimyjARHw',
   'Guillermo Chiacchio');

INSERT INTO user_roles (user_id, role_id) VALUES (5, 1);
INSERT INTO user_roles (user_id, role_id) VALUES (6, 1);