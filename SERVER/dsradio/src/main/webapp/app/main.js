/**
 * Bootstrap application
 */
require.config({
    waitSeconds: 15, //make sure it is enough to load all scripts
    // set paths since they are outside the baseUrl
    paths: {
        'underscore' : 'lib/underscore',
        'backbone': 'lib/backbone',
        'jquery': 'lib/jquery-1.11.0.min',
        'jqueryUI': 'lib/jquery-ui-1.10.4.custom.min',
        'bootstrap': 'lib/bootstrap.min',
        'Handlebars' : 'lib/handlebars',
        'hbs' : 'lib/hbs',
        'json2' : 'lib/json2',
        'i18nprecompile' : 'lib/i18nprecompile',
        'bootstrap-popover': 'lib/bootstrap-popover'
    },
    locale : "en_ca",
    // default plugin settings, listing here just as a reference
    hbs : {
        disableI18n: false,        // This disables the i18n helper and
        // doesn't require the json i18n files (e.g. en_us.json)
        // (false by default)

        templateExtension: "hbs" // Set the extension automatically appended to templates
        // ('hbs' by default)
    },

    map: {
        //From any module I get the views/models defined below
        '*': {
            //'aliasName': 'jsModeleFile'
        }
    },
    shim: {
        'jqueryUI': {
            deps: ['jquery'],
            exports: 'jqueryUI'
        },
        'underscore': {
            deps: [],
            exports: '_'
        },
        'backbone': {
            deps: ['jquery', 'underscore'],
            exports: 'Backbone'
        },
        'bootstrap': {
            deps: ['jquery', 'jqueryUI'],
            exports: 'bootstrap'
        },
        'bootstrap-popover': {
            deps: ['jquery', 'jqueryUI', 'bootstrap'],
            exports: 'bootstrap-popover'
        }
    }
});

define(
    [ 'backbone',
        'mediator/mediator',
        'widgets/radio.router'
    ],

    function (Backbone, Mediator, AppRouter) {
        'use strict';

        Backbone.View.prototype.close = function () {
            //We get the HTML that is currently populated inside of `this.el` removed from the DOM
            // (and therefore, removed from the visual portion of the application).
            // And we also get all of the DOM element events cleaned up for us.
            this.remove();
            // will unbind any events that our view triggers directly – that is, anytime we may have called
            //`this.trigger(…)` from within our view, in order to have our view raise an event.
            this.unbind();
            //This is necessary. The last thing we need to do, then, is unbind any model and collection
            //events that our view is bound to.
            //Each view should implement the onClose method or at least override ir with empty implementation
            if (this.onClose) {
                this.onClose();
                //I.e: this.model.unbind("change", this.render);
                // Mediator:
                // stopListening should be called if listenTo was used.
                // You SHOULD remove the `onChange` callbacks doing  this.mediator.off("change", onChange);
            } else {
                console.log('Warning, the view ' + this + ' is not overriding onClose method.');
            }
        };

        var app = new AppRouter({
            mediator: Mediator
        });

        //All the views that will live while the page is open should be declared here

        // End of views living while the app is running.

        Backbone.history.start();

    }
);
