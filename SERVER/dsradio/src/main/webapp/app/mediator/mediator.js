define(
    [
        'underscore',
        'backbone'
    ],
    function (_, Backbone) {

        'use strict';

        /**
         * @constructor Mediator to coordinate event between views
         * @extends Backbone.View
         */
        var Mediator = _.extend({}, Backbone.Events);

        return Mediator;

    }
);

