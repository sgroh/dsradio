define(
    [
        'backbone',
        './channel.model'
    ],
    function(Backbone, ChannelModel) {
        'use strict';
        var ChannelCollection = Backbone.Collection.extend({
            model : ChannelModel,
            url: 'channels?expand=stream',

            initialize: function (options) {
                this.options = options;
            }
        });

        return ChannelCollection;
    }
);