define(
    [
        'backbone',
        'jquery',
        'hbs!template/channelList'
    ],
    function (Backbone, $, resultTemplate) {
        'use strict';

        var ChannelCollectionView = Backbone.View.extend({

            initialize: function (options) {
                this.options = options;
            },

            render: function (channels) {
                this.$el.html(resultTemplate({channels: channels.items}));
                return this.$el;
            }
        });
        return ChannelCollectionView;
    }
);