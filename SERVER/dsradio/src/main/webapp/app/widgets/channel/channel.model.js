define(
    [
        'backbone'
    ],
    function (Backbone) {
        'use strict';

        var ChannelModel = Backbone.Model.extend({
                urlRoot: 'channels/',
                idAttribute: 'channelId'
            }
        );
        return ChannelModel;
    }
);