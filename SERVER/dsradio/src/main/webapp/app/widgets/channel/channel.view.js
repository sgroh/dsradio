define(
    [
        'backbone',
        'jquery',
        'hbs!template/channel'
    ],
    function (Backbone, $, resultTemplate) {
        'use strict';

        var ChannelView = Backbone.View.extend({
            events: {
            },

            initialize: function (options) {
                this.options = options;
                this.el = options.el;
                this.model = options.model;
                this.mediator = options.mediator;
                this.mediator.on('showChannel', this.showChannel, this);
            },

            showChannel: function (id) {
                this.render();
                var that = this;
                this.model.set({channelId: id});
                this.model.fetch({
                    success: function (channel) {
                        that.$el.html(resultTemplate({channel: channel.attributes}));
                    }
                })
            },

            render: function () {
                this.$el.html(resultTemplate());
            }
        });
        return ChannelView;
    }
);