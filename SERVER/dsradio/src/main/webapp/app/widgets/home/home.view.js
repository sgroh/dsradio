define(
    [
        'backbone',
        'jquery',
        'hbs!template/home'
    ],
    function (Backbone, $, resultTemplate) {
        'use strict';

        var HomeView = Backbone.View.extend({

            initialize: function (options) {
                this.options = options;
                this.el = options.el;
            },

            render: function () {
                this.$el.html(resultTemplate());
                this.$el;
            }
        });
        return HomeView;
    }
);