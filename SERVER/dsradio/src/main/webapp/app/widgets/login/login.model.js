define(
    [
        'backbone'
    ],
    function(Backbone) {
        'use strict';
        var LoginModel = Backbone.Model.extend({
            url:'j_spring_security_check',
            defaults : {
                j_username: '',
                j_password: ''
            }
        });
        return LoginModel;
    }
);
