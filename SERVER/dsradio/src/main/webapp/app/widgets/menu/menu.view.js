define(
    [
        'backbone',
        'jquery'
    ],
    function (Backbone, $) {
        'use strict';

        var MenuView = Backbone.View.extend({

            events: {
                'click ul.nav a': 'activeMenu'
            },

            initialize: function (options) {
                this.options = options;
                this.el = options.el;
                this.router = options.router;

                /**
                 * Handle the activation of the menu when router's navigate happens.
                 */
                this.router.bind('all', function () {
                    var url = window.location;
                    $('ul.nav li').removeClass('active');
                    $('ul.nav a').filter(function () {return this.href == url;} ).parent().addClass('active');
                });
            },

            /**
             * Handles the activation of the menu
             * @param event of click
             */
            activeMenu: function (event) {
                $('ul.nav li').removeClass('active');
                $(event.currentTarget).closest('li').addClass("active");
            },

            render: function () {
                return this;
            }
        });
        return MenuView;
    }
);