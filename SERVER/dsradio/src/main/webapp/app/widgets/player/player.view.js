define(
    [
        'backbone',
        'jquery',
        'hbs!template/player'
    ],
    function (Backbone, $, resultTemplate) {
        'use strict';

        var PlayerView = Backbone.View.extend({
            events: {
                'click .play': "playChannel"
            },

            initialize: function (options) {
                this.options = options;
                this.el = options.el;
                this.router = options.router;
                this.mediator = options.mediator;
                this.channelModel = options.model;
            },

            render: function (stream, channelId) {
                $('.player').html(resultTemplate({stream_url : stream}));
                $('.player').removeClass('deactivate');
                this.mediator.trigger("showChannel", channelId);
                 return this.$el;
            },

            playChannel: function(event){
                //TODO Check why it needs '//' in the path
                this.router.navigate("//player", true);
                this.render($(event.currentTarget).data('stream'), $(event.currentTarget).data('id'));
            }
        });
        return PlayerView;
    }
);