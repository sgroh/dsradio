define(
    [
        'backbone',
        'jquery',
        './channel/channel.model',
        './channel/channel.view',
        './login/login.model',
        './login/login.view',
        './menu/menu.model',
        './menu/menu.view',
        './channel/channel.collection',
        './channel/channel.collection.view',
        './player/player.view',
        './home/home.view',
        'bootstrap',
        'bootstrap-popover'
    ],
    function(Backbone, $, ChannelModel, ChannelView, LoginModel, LoginView, MenuModel, MenuView, ChannelCollection,
             ChannelCollectionView, PlayerView, HomeView){
        'use strict';
        /**
        * The router of all the radio app.
        * The idea is encourage a view manager to avoid memory leaks issues.
        *
        * @author jflores@devspark.com
        */
        var AppRouter = Backbone.Router.extend({
            /**
            * Maps of all the routes that could be used in the radio app
            */
            routes: {
                "": "showHome",
                "songDetails": "songDetails",
                "channels": "showChannels"
            },

            initialize: function  (options) {
                this.menuModel = new MenuModel();
                this.menuView = new MenuView({
                    model: this.menuModel,
                    el: '#bs-example-navbar-collapse-1',
                    router: this
                });

                this.channelModel = new ChannelModel();
                this.channelView = new ChannelView({
                    model: this.channelModel,
                    el: '#app',
                    mediator: options.mediator
                });

                this.channelCollection = new ChannelCollection();
                this.channelCollectionView = new ChannelCollectionView({
                    model: this.channelCollection,
                    el: '#channelsList'
                });

                this.playerView = new PlayerView({
                    el: '#app',
                    router: this,
                    mediator: options.mediator,
                    model: this.channelModel
                });

                this.homeView = new HomeView({
                    el: '#home-info'
                });
            },

            showChannels: function(){
                var self = this;
                this.channelCollection.fetch({
                    success: function (data, textStatus, jqXHR) {
                        $('#channelsList').html(self.channelCollectionView.render(textStatus).el);
                    }
                });
            },

            showHome: function () {
                this.homeView.render();
                this.showChannels();
            }
        });

    return AppRouter;
});


