package com.devspark.dsradio.mappers;

import com.devspark.dsradio.dto.ChannelDto;
import com.devspark.dsradio.dto.QueryParametersDto;
import com.devspark.dsradio.entities.Channel;
import com.devspark.dsradio.entities.ChannelSong;
import com.devspark.dsradio.entities.Stream;
import org.junit.Test;

import java.util.Arrays;
import java.util.Date;

import static org.junit.Assert.assertEquals;

/**
 * @author Federico Lenzi - flenzi@devspark.com
 */
public class ChannelMapperTest {

    @Test
    public void toDTO(){
//        ChannelSong channelSong1 = new ChannelSong("Song1", new Date());
//        channelSong1.setId(1L);
//        ChannelSong channelSong2 = new ChannelSong("Song2", new Date());
//        channelSong1.setId(2L);

        Stream stream = new Stream();
        stream.setUrl("channelUrl");

//        Channel channel = new Channel("ChannelName", stream, Arrays.asList(channelSong1,channelSong2));
        Channel channel = new Channel("ChannelName", stream);

        channel.setId(1L);

        ChannelDto channelDTO = new ChannelDto();

        QueryParametersDto queryParameters = new QueryParametersDto();
        queryParameters.setExpandables(Arrays.asList("stream"));

        ChannelMapper.toDTO(channel, channelDTO, queryParameters);
        assertEquals(channel.getId(), channelDTO.getId());
        assertEquals(channel.getName(), channelDTO.getName());
        assertEquals(channel.getStream().getUrl(), channelDTO.getStream().getUrl());

    }
}
