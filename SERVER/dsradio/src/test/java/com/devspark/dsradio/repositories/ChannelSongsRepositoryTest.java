package com.devspark.dsradio.repositories;

import com.devspark.dsradio.config.BaseConfigTest;
import com.devspark.dsradio.entities.Channel;
import com.devspark.dsradio.entities.ChannelSong;
import com.devspark.dsradio.entities.ChannelSongId;
import com.devspark.dsradio.entities.Song;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author Federico Lenzi - flenzi@devspark.com
 */
public class ChannelSongsRepositoryTest extends BaseConfigTest {

	@Inject
	private ChannelsRepository channelsRepository ;
	@Inject
	private ChannelSongsRepository channelSongRepository;
    @Inject
    private SongsRepository songsRepository;

	private Channel channel1;
	private Channel channel2;

    private Song song1;
    private Song song2;

    @Before
	public void initialize() {
		Channel channel1 = new Channel();
		channel1.setName("myChannel1");
        this.channel1 = channelsRepository.save(channel1);

        Channel channel2 = new Channel();
        channel2.setName("myChannel2");
        this.channel2 = channelsRepository.save(channel2);

        Song song1 = new Song();
        song1.setName("Game Of Expressions");
        this.song1 = songsRepository.save(song1);

        Song song2 = new Song();
        song2.setName("Overt Bible");
        this.song2 = songsRepository.save(song2);

        ChannelSong channelSong1 = new ChannelSong();
        channelSong1.setChannel(this.channel1);
        channelSong1.setSong(this.song1);
        channelSong1.setTime(new Date());
        channelSongRepository.save(channelSong1);

        ChannelSong channelSong2 = new ChannelSong();
        channelSong2.setChannel(this.channel1);
        channelSong2.setSong(this.song2);
        channelSongRepository.save(channelSong2);

        ChannelSong channelSong3 = new ChannelSong();
        channelSong3.setChannel(this.channel2);
        channelSong3.setSong(this.song1);
        channelSongRepository.save(channelSong3);
	}

	@Test
	public void retrieveSongChannel() {
        //Retrieve ChannelSongs by channelId
        List<ChannelSong> channelSongs1 = channelSongRepository.findAllByChannelId(channel1.getId());
        assertEquals(2, channelSongs1.size());

        List<ChannelSong> channelSongs2 = channelSongRepository.findAllByChannelId(channel2.getId());
        assertEquals(1, channelSongs2.size());

        //Retrieve ChannelSongs by songId
        List<ChannelSong> channelSongs3 = channelSongRepository.findAllBySongId(song1.getId());
        assertEquals(2, channelSongs3.size());

        List<ChannelSong> channelSongs4 = channelSongRepository.findAllBySongId(song2.getId());
        assertEquals(1, channelSongs4.size());
    }
}
