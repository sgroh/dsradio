package com.devspark.dsradio.repositories;

import com.devspark.dsradio.config.BaseConfigTest;
import com.devspark.dsradio.entities.Song;
import com.devspark.dsradio.entities.User;
import com.devspark.dsradio.entities.UserSong;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author Federico Lenzi - flenzi@devspark.com
 */
public class UserLikesSongTest extends BaseConfigTest {
	@Inject
	private SongsRepository songsRepository;

	@Inject
	private UsersRepository userRepository;

	@Inject
	private UserSongRepository userSongRepository;

	private Long userId;
	
	@Before
	public void initialize() {
		User user = new User();
		user.setUserName("Peter");
		User savedUser = userRepository.save(user);
		userId = savedUser.getId();

		Song song = new Song();
		song.setAuthor("La mona Gimenez");

		UserSong userSong = new UserSong();
		userSong.setUser(savedUser);
		userSong.setSong(song);
		userSong.setUserLiked(true); // extra column

		song.setUserLiked(Arrays.asList(userSong));

		songsRepository.save(song);

	}

	@Test
	public void userLikes() {
		User user = userRepository.findByIdWithLikes(userId);
		assertEquals(1, user.getSongsLiked().size());
		assertEquals("Peter", user.getUsername());
	}
	
	@Test
	public void userLikesUserSongRepository() {
		List<UserSong> userSongs = userSongRepository.findUserLikes(userId);
		UserSong userSong = userSongs.get(0);
		
		assertEquals("Peter", userSong.getUser().getUsername());
		assertEquals("La mona Gimenez", userSong.getSong().getAuthor());

	}


}