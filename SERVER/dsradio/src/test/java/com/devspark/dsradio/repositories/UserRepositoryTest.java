package com.devspark.dsradio.repositories;

import com.devspark.dsradio.config.BaseConfigTest;
import com.devspark.dsradio.entities.Suggestion;
import com.devspark.dsradio.entities.User;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;

/**
 * @author Federico Lenzi - flenzi@devspark.com
 */
public class UserRepositoryTest extends BaseConfigTest {

	@Inject
	private UsersRepository userRepository;
	
	private Long userId;

	@Before
	public void initialize() {
		User user = new User();
		user.setUserName("John");

		Suggestion suggestion1 = new Suggestion();
		suggestion1.setSongName("One");
		suggestion1.setUser(user);

		Suggestion suggestion2 = new Suggestion();
		suggestion2.setSongName("Lunita Tucumana");
		suggestion2.setUser(user);

		user.setSuggestions(Arrays.asList(suggestion1, suggestion2));

		User savedUser = userRepository.save(user);
		
		userId = savedUser.getId();

	}

	@Test
	public void getUserSuggestions() {
		User retrievedUserWithSuggestions = userRepository.findByIdWithSuggestions(userId);

		assertEquals("John", retrievedUserWithSuggestions.getUsername());
		assertEquals(2, retrievedUserWithSuggestions.getSuggestions().size());
		
	}

}