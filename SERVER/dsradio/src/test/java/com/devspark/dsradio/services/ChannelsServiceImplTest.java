package com.devspark.dsradio.services;

import com.devspark.dsradio.dto.ChannelDto;
import com.devspark.dsradio.entities.Channel;
import com.devspark.dsradio.entities.Stream;
import com.devspark.dsradio.mappers.ChannelMapper;
import com.devspark.dsradio.repositories.ChannelSongsRepository;
import com.devspark.dsradio.repositories.ChannelsRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Federico Lenzi - flenzi@devspark.com
 */
public class ChannelsServiceImplTest {

    @Mock
    private ChannelsRepository channelsRepository;

    @Mock
    private ChannelSongsRepository channelSongsRepository;

    @Mock
    private UserService userService;

    private ChannelService channelsService;

    private Channel channel;
    private List<Channel> channels;
    private ChannelDto channelDto;
    private List<ChannelDto> channelDtos;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        channelsService = new ChannelServiceImpl(channelsRepository, channelSongsRepository, userService);
        setupChannels();
    }

    /**
     * Instantiates the channels
     */
    private void setupChannels() {
        channel = new Channel();
        channel.setId(1L);
        channel.setName("myChannel1");
        Stream stream1 = new Stream();
        stream1.setId(1L);
        stream1.setUrl("http://urll1");
        channel.setStream(stream1);

        Channel channel2 = new Channel();
        channel2.setId(2L);
        channel2.setName("myChannel2");
        Stream stream2 = new Stream();
        stream2.setId(2L);
        stream2.setUrl("http://urll2");
        channel2.setStream(stream2);

        channelDto = new ChannelDto();
        ChannelMapper.toDTO(channel, channelDto);

        channels = new ArrayList<>();
        channels.add(channel);
        channels.add(channel2);

        channelDtos = new ArrayList<>();
        ChannelMapper.toDTO(channels, channelDtos);
    }

    @Test
    public void testGetChannel() {
        when(channelsRepository.findOne(1L)).thenReturn(channel);
        when(channelSongsRepository.findAllByChannelId(any(Long.class))).thenReturn(null);
        ChannelDto ch = channelsService.getChannelById(1L);

        verify(channelsRepository, times(1)).findOne(any(Long.class));
        assertEquals(channel.getName(), ch.getName());
    }

    @Test
    public void testAddChannel() {
        when(channelsRepository.save(any(Channel.class))).thenReturn(channel);
        ChannelDto ch = channelsService.createChannel(channelDto);

        verify(channelsRepository, times(1)).save(ChannelMapper.fromDTO(channelDto, new Channel()));
        assertEquals(channel.getName(), ch.getName());
    }

    @Test
    public void testDeleteChannel() {
        when(channelsRepository.findOne(1L)).thenReturn(channel);
        channelsService.deleteChannel(1L);

        verify(channelsRepository, times(1)).delete(channel);
    }

    @Test
    public void testGetChannels() {
        when(channelsRepository.findAll()).thenReturn(channels);
        channelDtos = channelsService.getChannels();

        verify(channelsRepository, times(1)).findAll();
        compareChannels(channels, channelDtos);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testEntityNotFound() throws Exception {
        when(channelsRepository.findOne(any(Long.class))).thenReturn(null);
        channelsService.getChannelById(1L);
    }

    private void compareChannels(List<Channel> channels, List<ChannelDto> channelDtos) {
        for (int i = 0; i < this.channelDtos.size(); i++) {
            assertEquals(this.channels.get(i).getName(), this.channelDtos.get(i).getName());
            assertEquals(this.channels.get(i).getId(), this.channelDtos.get(i).getId());
        }
    }
}
