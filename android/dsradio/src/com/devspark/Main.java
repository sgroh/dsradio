package com.devspark;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.devspark.fragment.PlayerFragment;
import com.devspark.fragment.SuggestionFragment;
import com.devspark.fragment.TabsAdapter;
import com.example.dsradio.R;

public class Main extends SherlockFragmentActivity {

    private static final String PLAYER_TAB_LABEL = "Player";
    private static final String FEEDBACK_TAB_LABEL = "Feedback";
    private static final String PLAYER_TAB = "player";
    private static final String FEEDBACK_TAB = "feedback";
    private ViewPager mViewPager;
    private TabsAdapter mTabsAdapter;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        mViewPager = (ViewPager) findViewById(R.id.pager);

        // The following two options trigger the collapsing of the main action bar view.
        // See the parent activity for the rest of the implementation
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        getSupportActionBar().setNavigationMode(com.actionbarsherlock.app.ActionBar.NAVIGATION_MODE_TABS);

        mTabsAdapter = new TabsAdapter(this, mViewPager);
        mTabsAdapter.addTab(PLAYER_TAB, PLAYER_TAB_LABEL, PlayerFragment.class, null);
        mTabsAdapter.addTab(FEEDBACK_TAB, FEEDBACK_TAB_LABEL, SuggestionFragment.class, null);

    }

}
