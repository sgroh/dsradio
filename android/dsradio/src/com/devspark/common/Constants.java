package com.devspark.common;

/**
 * Constants to be used on the DevSpark Radio
 * 
 * @author sgroh@devspark.com
 */
public interface Constants {

    public static final String SERVICE_BROADCAST_ACTION = "devspark_broadcast_action";

    public static final String UPDATE_STATUS = "update_status";

    public static final String UPDATE_TITLE = "update_title";

    public static final String NEW_SONG = "new_song";

    public static final String STREAMING_FLAG = "isStreaming";

    public static final String STREAMING_ARTIST = "streamArtist";

    public static final String STREAMING_SONG = "streamSong";

    public static final String TAB_CHANGED = "tabChanged";

    public static final String UPDATE_PLAYING_SONG = "updatePlayingSong";
}
