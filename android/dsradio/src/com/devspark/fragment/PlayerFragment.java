package com.devspark.fragment;

import android.content.*;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import com.actionbarsherlock.app.SherlockFragment;
import com.devspark.player.RadioStreamer;
import com.devspark.player.Song;
import com.devspark.player.SongManager;
import com.devspark.common.Constants;
import com.devspark.social.VoteButton;
import com.devspark.social.VoteClickListener;
import com.example.dsradio.R;

public class PlayerFragment extends SherlockFragment {

    private static final String M_STATUS_TEXT = "mStatusText";
    private static final String M_ARTIST_TEXT = "artistText";
    private static final String M_SONG_TEXT = "songText";
    private static TextView mStatusText;
    private static TextView artistTextView;
    private static TextView songTextView;
    private static ImageButton playButton;
    private static ImageButton stopButton;
    private static VoteButton likeButton;
    private static VoteButton dislikeButton;
    private static final String STREAM_URI = "http://streaming.radionomy.com/devspark";
    private RadioStreamer radioStreamerService;
    final IntentFilter serviceStatusFilter = new IntentFilter(Constants.SERVICE_BROADCAST_ACTION);
    final IntentFilter serviceTitleFilter = new IntentFilter(Constants.SERVICE_BROADCAST_ACTION);
    final IntentFilter tabChangeFilter = new IntentFilter(Constants.SERVICE_BROADCAST_ACTION);
    private StatusReceiver statusReceiver = new StatusReceiver();
    private TitleReceiver titleReceiver = new TitleReceiver();
    private TabChangeUpdater tabChangeUpdater = new TabChangeUpdater();
    private static SongManager songManager= SongManager.getInstance();

    /**
     * Inflates the current fragment with the layout specified.
     * 
     * @param inflater
     *            the inflater object
     * @param container
     *            the container object
     * @param savedInstanceState
     *            the Bundle object, not used here.
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.player_layout, container, false);
        Intent playerIntent = new Intent(this.getActivity(), RadioStreamer.class);
        playerIntent.setData(Uri.parse(STREAM_URI));
        serviceStatusFilter.addCategory(Constants.UPDATE_STATUS);
        serviceTitleFilter.addCategory(Constants.UPDATE_TITLE);
        tabChangeFilter.addCategory(Constants.TAB_CHANGED);
        this.getActivity().registerReceiver(statusReceiver, serviceStatusFilter);
        this.getActivity().registerReceiver(titleReceiver, serviceTitleFilter);
        this.getActivity().registerReceiver(tabChangeUpdater, tabChangeFilter);
        this.getActivity().bindService(playerIntent, mConnection, Context.BIND_AUTO_CREATE);
        return rootView;
    }

    /**
     * The service connection object to bind the Streaming service.
     */
    private ServiceConnection mConnection = new ServiceConnection() {
        /**
         * Called when the connection with the service {@link RadioStreamer} has been established. The instance is
         * maintained in the @{link this.radioStreamerService} property.
         * 
         * @param className
         *            not being used
         * @param service
         *            used to receive a {@link com.devspark.player.RadioStreamer.RadioStreamingBinder}
         */
        public void onServiceConnected(ComponentName className, IBinder service) {
            radioStreamerService = ((RadioStreamer.RadioStreamingBinder) service).getService();
        }

        /**
         * Called when the connection with the service {@link RadioStreamer} has been unexpectedly disconnected.
         * 
         * @param className
         *            not being used
         */
        public void onServiceDisconnected(ComponentName className) {
            radioStreamerService = null;
        }
    };

    /**
     * Called when the activity is first created.
     * 
     * @param savedInstanceState
     *            the bundle used to store previous information. The information is lost when the screen is rotated or
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mStatusText = (TextView) getActivity().findViewById(R.id.status_txt);
        artistTextView = (TextView) getActivity().findViewById(R.id.artist_txt);
        songTextView = (TextView) getActivity().findViewById(R.id.song_txt);
        playButton = (ImageButton) getActivity().findViewById(R.id.play_button);
        stopButton = (ImageButton) getActivity().findViewById(R.id.stop_button);
        likeButton = (VoteButton) getActivity().findViewById(R.id.like_button);
        dislikeButton = (VoteButton) getActivity().findViewById(R.id.dislike_button);
        likeButton.setPositive(true);
        dislikeButton.setPositive(false);
        View.OnClickListener playClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mStatusText.setText(getResources().getString(R.string.preparing_stream));
                radioStreamerService.playStream();
            }
        };
        View.OnClickListener stopClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mStatusText.setText(getResources().getString(R.string.stopping_stream));
                radioStreamerService.stopStream();
            }
        };
        playButton.setOnClickListener(playClickListener);
        stopButton.setOnClickListener(stopClickListener);
        restoreInstanceState(savedInstanceState);
    }

    /**
     * Save UI state changes to the savedInstanceState.
     * 
     * @param savedInstanceState
     *            bundle that arrives on {@link this.onCreate} method. Used if the process is killed and restarted.
     */
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {

        // Save UI state changes to the savedInstanceState.
        // This bundle will be passed to onCreate if the process is
        // killed and restarted.
        savedInstanceState.putString(M_STATUS_TEXT, mStatusText.getText().toString());
        savedInstanceState.putString(M_ARTIST_TEXT, artistTextView.getText().toString());
        savedInstanceState.putString(M_SONG_TEXT, songTextView.getText().toString());
        super.onSaveInstanceState(savedInstanceState);
    }

    /**
     * Restore UI state from the savedInstanceState.
     * 
     * @param savedInstanceState
     *            the Bundle that should contain the information to be restored.
     */
    private void restoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            mStatusText.setText(savedInstanceState.getString(M_STATUS_TEXT));
            artistTextView.setText(savedInstanceState.getString(M_ARTIST_TEXT));
            songTextView.setText(savedInstanceState.getString(M_SONG_TEXT));
            setVoteButtonsVisible();
        }
    }

    /**
     * Broadcast receiver for receiving status updates.
     * 
     * The status could be streaming or not streaming.
     */
    private class StatusReceiver extends BroadcastReceiver {

        /**
         * This method is called when some {@link com.devspark.player.RadioStreamer} events are triggered.
         * 
         * @param context
         *            required to override, not used here.
         * @param intent
         *            the intent sent from the {@link com.devspark.player.RadioStreamer#sendBroadcastStreamingMessage(boolean)}
         *            method.
         */
        @Override
        public void onReceive(final Context context, final Intent intent) {
            if (intent == null) {
                mStatusText.setText(R.string.stream_error);
            } else {
                if (intent.getBooleanExtra(Constants.STREAMING_FLAG, true)) {
                    mStatusText.setText(R.string.playing_stream);
                } else {
                    mStatusText.setText(R.string.stream_stopped);
                }
            }
        }
    };

    /**
     * Broadcast receiver for receiving tab changes.
     *
     * The Vote buttos should be updated.
     */
    private class TabChangeUpdater extends BroadcastReceiver {
    /**
     * This method is called when the {@link com.devspark.fragment.TabsAdapter} detects user changes.
     *
     * @param context
     *            required to override, not used here.
     * @param intent
     *            the intent sent from the {@link TabsAdapter#broadcastViewChanged()} method.
     */
    @Override
    public void onReceive(final Context context, final Intent intent) {
        if (intent != null) {
            Song song = songManager.reproducingNow();
            if ( song!= null ) {
                if (song.getLocalVote()==0) {
                    likeButton.setImageResource(R.drawable.like);
                    dislikeButton.setImageResource(R.drawable.dislike);
                    likeButton.setVisibility(View.VISIBLE);
                    dislikeButton.setVisibility(View.VISIBLE);
                }else if (song.getLocalVote()==1) {
                    likeButton.setImageResource(R.drawable.like_voted);
                    dislikeButton.setVisibility(View.INVISIBLE);
                } else if (song.getLocalVote()==-1) {
                    dislikeButton.setImageResource(R.drawable.dislike_voted);
                    likeButton.setVisibility(View.INVISIBLE);
                }
            }
        }
    }
};


    /**
     * Broadcast receiver for receiving song titles updates.
     * 
     * The status could be streaming or not streaming.
     */
    private class TitleReceiver extends BroadcastReceiver {

        /**
         * This method is called when some {@link com.devspark.player.RadioStreamer} events are triggered.
         * 
         * @param context
         *            required to override, not used here.
         * @param intent
         *            the intent sent from the {@link com.devspark.player.RadioStreamer#sendBroadcastStreamingMessage(boolean)}
         *            method.
         */
        @Override
        public void onReceive(final Context context, final Intent intent) {
            if (intent == null) {
                artistTextView.setText("");
                songTextView.setText("");
            } else {
                Song actualSong = songManager.reproducingNow();
                String retrievedArtist = intent.getStringExtra(Constants.STREAMING_ARTIST);
                String retrievedSong = intent.getStringExtra(Constants.STREAMING_SONG);
                if (actualSong==null ||
                    (retrievedArtist != null  && retrievedArtist.length() > 0)) {
                    artistTextView.setText(getResources().getString(R.string.artist) + " " + retrievedArtist);
                }
                if (actualSong==null ||
                    (retrievedSong != null
                        && retrievedSong.length() > 0)) {
                    songTextView.setText(getResources().getString(R.string.song) + " " + retrievedSong);
                }
                try {
                    Song song = songManager.addSong(retrievedArtist, retrievedSong);
                    if (song != null) {
                        VoteClickListener likeClickListener = new VoteClickListener(R.drawable.like_voted,
                                dislikeButton, song, Constants.UPDATE_PLAYING_SONG, getActivity());
                        VoteClickListener dislikeClickListener = new VoteClickListener(R.drawable.dislike_voted,
                                likeButton, song, Constants.UPDATE_PLAYING_SONG, getActivity());
                        likeButton.setOnClickListener(likeClickListener);
                        dislikeButton.setOnClickListener(dislikeClickListener);
                        setVoteButtonsVisible();
                        broadcastNewSong();
                    }
                } catch (IllegalArgumentException e) {
                    artistTextView.setText(getResources().getString(R.string.metadata_invalid));
                }
            }
        }

        /**
         * Broadcast that a new song was added.
         * A new intent is created to notify that a new song has been detected in the Stream metadata.
         */
        private void broadcastNewSong(){
            final Intent intent = new Intent();
            intent.setAction(Constants.SERVICE_BROADCAST_ACTION);
            intent.addCategory(Constants.NEW_SONG);
            getActivity().sendBroadcast(intent);
        }
    };

    /**
     * Shown the Like and DisLike buttons if the stream is present.
     */
    private void setVoteButtonsVisible() {
        if (artistTextView.getText().length() + songTextView.getText().length() > 0) {
            likeButton.setImageResource(R.drawable.like);
            dislikeButton.setImageResource(R.drawable.dislike);
            likeButton.setVisibility(View.VISIBLE);
            dislikeButton.setVisibility(View.VISIBLE);
        }
    }
}