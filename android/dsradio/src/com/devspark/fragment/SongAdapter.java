package com.devspark.fragment;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.devspark.common.Constants;
import com.devspark.player.Song;
import com.devspark.social.VoteButton;
import com.devspark.social.VoteClickListener;
import com.example.dsradio.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Songs adapter to present the different songs.
 *
 * @author sgroh@devspark.com
 */
public class SongAdapter extends ArrayAdapter<Song> {
    /**
     * The model of this adapter.
     */
    public List<Song> songs;
    /**
     * The context from where this adapter was created.
     */
    private Context context;
    /**
     * Max number of allowed rows.
     */
    private int maxCapacity;

    /**
     * Default Constructor.
     *
     * @param context the context from where it was created.
     * @param resource the resource required to draw a row. In this case {@see played_song_layout}.
     * @param songs the list of played songs.
     * @param maxCapacity the max number of allowed rows.
     */
    public SongAdapter(Context context, int resource, List<Song> songs, int maxCapacity) {
        super(context, resource, songs);
        this.songs = songs;
        this.context = context;
        this.maxCapacity = maxCapacity;
    }

    /**
     * Add the song that is being played.
     *
     * @param song the {@link com.devspark.player.Song} object.
     */
    @Override
    public void add(Song song) {
        songs.add(song);
        if (songs.size() > maxCapacity){
            songs.remove(0);
        }
        notifyDataSetChanged();
    }

    /**
     * Returns the row view to be presented in the scrollable area.
     *
     * @param position the ordinal position inside the ListView
     * @param rowView the existent Row view in the position to be rendered.
     * @param parent the ViewGroup parent.
     * @return
     */
    @Override
    public View getView(int position, View rowView, ViewGroup parent) {
        if (rowView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            rowView = inflater.inflate(R.layout.played_song_layout, null, true);
        }
        Song song = songs.get(songs.size() - 1 - position);
        if (song!=null){
            TextView artistTextView = (TextView) rowView.findViewById(R.id.played_artist_txt);
            TextView songTextView = (TextView) rowView.findViewById(R.id.played_song_txt);
            artistTextView.setText(song.getArtist());
            songTextView.setText(song.getName());
            ImageView playingIcon = (ImageView) rowView.findViewById(R.id.played_song_image);
            if (position==0) {
                playingIcon.setVisibility(View.VISIBLE);
            } else {
                playingIcon.setVisibility(View.INVISIBLE);
            }
            VoteButton likeButton = (VoteButton) rowView.findViewById(R.id.like_small_button);
            VoteButton dislikeButton = (VoteButton) rowView.findViewById(R.id.dislike_small_button);
            likeButton.setPositive(true);
            dislikeButton.setPositive(false);
            // check if the vote has already done for this song
            if (song.getLocalVote() == 0) {
                VoteClickListener likeClickListener = new VoteClickListener(R.drawable.like_small_voted, dislikeButton,
                        song, Constants.TAB_CHANGED, context);
                VoteClickListener dislikeClickListener = new VoteClickListener(R.drawable.dislike_small_voted,
                        likeButton, song, Constants.TAB_CHANGED, context);
                likeButton.setOnClickListener(likeClickListener);
                dislikeButton.setOnClickListener(dislikeClickListener);
                likeButton.setImageResource(R.drawable.like_small);
                dislikeButton.setImageResource(R.drawable.dislike_small);
                likeButton.setVisibility(View.VISIBLE);
                dislikeButton.setVisibility(View.VISIBLE);
            } else if (song.getLocalVote() == 1) {
                likeButton.setImageResource(R.drawable.like_small_voted);
                likeButton.setVisibility(View.VISIBLE);
                dislikeButton.setVisibility(View.INVISIBLE);
            } else if (song.getLocalVote() == -1) {
                dislikeButton.setImageResource(R.drawable.dislike_small_voted);
                dislikeButton.setVisibility(View.VISIBLE);
                likeButton.setVisibility(View.INVISIBLE);
            }
        }
        return rowView;
    }

    /**
     * The amount of different songs in the {@link android.widget.ListView} .
     *
     * @return the amount of different songs played.
     */
    @Override
    public int getCount() {
        return songs.size();
    }

    /**
     * Return the song in the position required.
     *
     * @param position the ordinal position inside the view.
     */
    @Override
    public Song getItem(int position) {
        return songs.get(position);
    }
}
