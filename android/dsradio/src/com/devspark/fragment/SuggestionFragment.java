package com.devspark.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import com.actionbarsherlock.app.SherlockListFragment;
import com.devspark.common.Constants;
import com.devspark.player.Song;
import com.devspark.player.SongManager;
import com.example.dsradio.R;

/**
 * This fragment contains the list of played songs and the suggestion songs section.
 * 
 * @author sgroh@devspark.com
 */
public class SuggestionFragment extends SherlockListFragment implements AdapterView.OnItemClickListener {
    private static SongManager songManager= SongManager.getInstance();
    final IntentFilter newSongFilter = new IntentFilter(Constants.SERVICE_BROADCAST_ACTION);
    private SongReceiver songReceiver = new SongReceiver();
    private ArrayAdapter<Song> songsAdapter;
    final IntentFilter tabChangeFilter = new IntentFilter(Constants.SERVICE_BROADCAST_ACTION);
    private TabChangeUpdater tabChangeUpdater = new TabChangeUpdater();

    /**
     * Method called every time that the Fragment is created.
     *
     * @param inflater the infalter used to create the feedbackLayout.
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.feedback_layout, container, false);
        songsAdapter = new SongAdapter(getActivity(), R.layout.played_song_layout, songManager.getPlayedSongs(), songManager.capacity());
        setListAdapter(songsAdapter);
        newSongFilter.addCategory(Constants.NEW_SONG);
        this.getActivity().registerReceiver(songReceiver, newSongFilter);
        tabChangeFilter.addCategory(Constants.UPDATE_PLAYING_SONG);
        this.getActivity().registerReceiver(tabChangeUpdater, tabChangeFilter);
        return view;
    }

    /**
     * Method invoked when the activity is created.
     *
     * @param savedInstanceState the bundle to restore values if needed.
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getListView().setOnItemClickListener(this);
    }

    /**
     * Callback method to be invoked when an item in this AdapterView has
     * been clicked.
     *
     * @param parent The AdapterView where the click happened.
     * @param view The view within the AdapterView that was clicked (this
     *            will be a view provided by the adapter)
     * @param position The position of the view in the adapter.
     * @param id The row id of the item that was clicked.
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }


    /**
     * Broadcast receiver for receiving a new song update.
     *
     */
    private class SongReceiver extends BroadcastReceiver {

        /**
         * This method is called when some {@link com.devspark.fragment.PlayerFragment.TitleReceiver#broadcastNewSong()} events are triggered.
         *
         * @param context
         *            required to override, not used here.
         * @param intent
         *            the intent sent from the {@link com.devspark.player.RadioStreamer#sendBroadcastStreamingMessage(boolean)}
         *            method.
         */
        @Override
        public void onReceive(final Context context, final Intent intent) {
            if (intent != null) {
               songsAdapter.add(songManager.reproducingNow());
            }
        }
    };

    /**
     * Broadcast receiver for receiving tab changes.
     *
     * The Vote buttos should be updated.
     */
    private class TabChangeUpdater extends BroadcastReceiver {
        /**
         * This method is called when the {@link com.devspark.fragment.TabsAdapter} detects user changes.
         *
         * @param context
         *            required to override, not used here.
         * @param intent
         *            the intent sent from the {@link TabsAdapter#broadcastViewChanged()} method.
         */
        @Override
        public void onReceive(final Context context, final Intent intent) {
            if (intent != null) {
                songsAdapter.notifyDataSetChanged();
            }
        }
    };
}