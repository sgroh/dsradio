package com.devspark.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TabHost;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.devspark.common.Constants;

import java.util.ArrayList;

public class TabsAdapter extends FragmentPagerAdapter implements ViewPager.OnPageChangeListener, ActionBar.TabListener {
    private final SherlockFragmentActivity mContext;
    private final ViewPager mViewPager;
    private final ArrayList<TabInfo> mTabs = new ArrayList<TabInfo>();

    static final class TabInfo {
        @SuppressWarnings("unused")
        private final String tag;
        private final Class<?> clss;
        private final Bundle args;

        TabInfo(String _tag, Class<?> _class, Bundle _args) {
            tag = _tag;
            clss = _class;
            args = _args;
        }
    }

    public TabsAdapter(SherlockFragmentActivity activity, ViewPager pager) {
        super(activity.getSupportFragmentManager());
        mContext = activity;
        mViewPager = pager;
        mViewPager.setAdapter(this);
        mViewPager.setOnPageChangeListener(this);
    }

    public void addTab(String tag, CharSequence label, Class<?> clss, Bundle args) {
        ActionBar.Tab tab = mContext.getSupportActionBar().newTab();
        tab.setText(label);
        tab.setTabListener(this);
        mContext.getSupportActionBar().addTab(tab);
        TabInfo info = new TabInfo(tag, clss, args);
        mTabs.add(info);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mTabs.size();
    }

    @Override
    public Fragment getItem(int position) {
        TabInfo info = mTabs.get(position);
        return Fragment.instantiate(mContext, info.clss.getName(), info.args);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        mContext.getSupportActionBar().setSelectedNavigationItem(position);
        broadcastViewChanged();
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.actionbarsherlock.app.ActionBar.TabListener#onTabSelected(com.actionbarsherlock.app.ActionBar.Tab,
     * android.support.v4.app.FragmentTransaction)
     */
    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
        mViewPager.setCurrentItem(mContext.getSupportActionBar().getSelectedNavigationIndex());
        broadcastViewChanged();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.actionbarsherlock.app.ActionBar.TabListener#onTabUnselected(com.actionbarsherlock.app.ActionBar.Tab,
     * android.support.v4.app.FragmentTransaction)
     */
    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.actionbarsherlock.app.ActionBar.TabListener#onTabReselected(com.actionbarsherlock.app.ActionBar.Tab,
     * android.support.v4.app.FragmentTransaction)
     */
    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {
    }

    /**
     * Broadcast that the user select another tab.
     * Vote buttons should be updated.
     */
    private void broadcastViewChanged() {
            final Intent intent = new Intent();
            intent.setAction(Constants.SERVICE_BROADCAST_ACTION);
            intent.addCategory(Constants.TAB_CHANGED);
            mContext.sendBroadcast(intent);
    }

}
