package com.devspark.player;

import android.os.AsyncTask;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class obtains the ICY metadata of the stream..
 * 
 * @author sgroh@devspark.com
 */
public class IcyMetadata extends AsyncTask<String, Void, Map> {

    private RadioStreamer radioStreamer;
    private static String STREAM_TITLE = "StreamTitle";
    private static String META_PARTS_SPLIT_CHARACTER = ";";
    private static String ICY_REQUEST_PROPERTY = "Icy-MetaData";
    private static String ICY_REQUEST_PROPERTY_ACTIVE = "1";
    private static String ICY_METAINT_PROPERTY = "icy-metaint";
    private static int MAX_COUNT_CHAR_TO_READ = 16320;
    private static int MAX_META_DATA_LENGTH = 4080; // 4080 is the max length

    /**
     * Default constructor.
     */
    public IcyMetadata(RadioStreamer radioStreamer) {
        this.radioStreamer = radioStreamer;
    }

    /**
     * Gets the stream title
     * 
     * @return String containing the title if present.
     */
    public String getTitle(Map<String, String> metadata) {
        if (!metadata.containsKey(STREAM_TITLE))
            return "";

        String streamTitle = metadata.get(STREAM_TITLE);
        return streamTitle.trim();
    }

    /**
     * Parse the readed metadata using a Regex.
     * 
     * All chains like "key=value" inside the specified metaString parameter will be detected and added into a Map that
     * will be the returned.
     * 
     * @param metaString
     *            the metadata String
     * @return a map with the information found.
     */
    public static Map<String, String> parseMetadata(String metaString) {
        Map<String, String> metadata = new HashMap();
        String[] metaParts = metaString.split(META_PARTS_SPLIT_CHARACTER);
        Pattern p = Pattern.compile("^([a-zA-Z]+)=\\'([\\x00-\\x7F\\']*)\\'$"); //all the ASCII characters
        Matcher m;
        for (int i = 0; i < metaParts.length; i++) {
            m = p.matcher(metaParts[i]);
            if (m.find()) {
                metadata.put((String) m.group(1), (String) m.group(2));
            }
        }
        return metadata;
    }

    /**
     * Called when execute method on this class is called.
     * 
     * @param urls
     *            The url list to be invoked, in this case we are going to use only the first one.
     * @return a Map with the ICY information
     */
    @Override
    protected Map doInBackground(String... urls) {
        try {
            URL url = new URL(urls[0]);
            URLConnection con = url.openConnection();
            con.setRequestProperty(ICY_REQUEST_PROPERTY, ICY_REQUEST_PROPERTY_ACTIVE);
            con.connect();

            int metaDataOffset = 0;
            Map<String, List<String>> headers = con.getHeaderFields();
            InputStream stream = con.getInputStream();

            if (headers.containsKey(ICY_METAINT_PROPERTY)) { // Headers that are sent via HTTP
                metaDataOffset = Integer.parseInt(headers.get(ICY_METAINT_PROPERTY).get(0));
            } else { // Headers are sent within a stream
                StringBuilder strHeaders = new StringBuilder();
                char c;
                while ((c = (char) stream.read()) != -1) {
                    strHeaders.append(c);
                    if (strHeaders.length() > MAX_COUNT_CHAR_TO_READ
                            || (strHeaders.length() > 5 && (strHeaders.substring((strHeaders.length() - 4),
                                    strHeaders.length()).equals("\r\n\r\n")))) {
                        // end of headers
                        break;
                    }
                }
                // Match headers to get metadata offset within a stream
                Pattern p = Pattern.compile("\\r\\n(" + ICY_METAINT_PROPERTY + "):\\s*(.*)\\r\\n");
                Matcher m = p.matcher(strHeaders.toString());
                if (m.find()) {
                    metaDataOffset = Integer.parseInt(m.group(2));
                }
            }
            // In case no data was sent
            if (metaDataOffset == 0) {
                return new HashMap();
            }

            // Read metadata
            int b;
            int count = 0;
            int metaDataLength = MAX_META_DATA_LENGTH;
            StringBuilder metaData = new StringBuilder();
            // Stream position should be either at the beginning or right after headers
            while ((b = stream.read()) != -1) {
                count++;
                // Length of the metadata
                if (count == metaDataOffset + 1) {
                    metaDataLength = b * 16;
                }
                if (count > metaDataOffset + 1 && count < (metaDataOffset + metaDataLength) && b != 0) {
                    metaData.append((char) b);
                }
                if (count > (metaDataOffset + metaDataLength)) {
                    break;
                }

            }
            stream.close();
            return IcyMetadata.parseMetadata(metaData.toString());
        } catch (IOException e) {
            return new HashMap();
        }
    }

    /**
     * When the thread ends running this method is called and the service is updated.
     * 
     * @param result
     *            the Map with the ICY information
     */
    protected void onPostExecute(Map result) {
        if (radioStreamer != null) {
            radioStreamer.setSongInformation(getTitle(result));
        }
    }
}
