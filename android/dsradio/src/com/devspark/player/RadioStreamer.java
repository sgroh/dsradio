package com.devspark.player;

import android.app.Service;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import com.devspark.common.Constants;

import java.io.IOException;

public class RadioStreamer extends Service implements MediaPlayer.OnInfoListener, MediaPlayer.OnPreparedListener,
        MediaPlayer.OnErrorListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnBufferingUpdateListener {

    private MediaPlayer mediaPlayer;
    private static String STREAM_URI;
    private final IBinder mBinder = new RadioStreamingBinder();
    private static String LOG_TAG = "DSRadio";
    private static String DEFAULT_ERROR_MSG = "Media Player Error: Non standard (-1) -1";

    /**
     * Method called after call {@link RadioStreamer#onCreate()}
     * 
     * @param intent
     *            the Intent created from the Activity {@link com.devspark.Main}
     * @return a {@link RadioStreamer.RadioStreamingBinder} object.
     */
    @Override
    public IBinder onBind(Intent intent) {
        String streamURI = intent.getDataString();
        STREAM_URI = streamURI;
        try {
            mediaPlayer.setDataSource(STREAM_URI);
        } catch (IllegalArgumentException e) {
            nonStandardError();
        } catch (IllegalStateException e) {
            nonStandardError();
        } catch (IOException e) {
            nonStandardError();
        }
        return mBinder;
    }

    /**
     * Logs a default error message;
     */
    private void nonStandardError() {
        Log.e(LOG_TAG, DEFAULT_ERROR_MSG);
    }

    /**
     * Binder class used to return this service to some activity, in this case {@link com.devspark.Main}
     */
    public class RadioStreamingBinder extends Binder {
        public RadioStreamer getService() {
            return RadioStreamer.this;
        }
    }

    /**
     * This method is called when the service is binded. Creates the player object and also add the needed listeners.
     */
    @Override
    public void onCreate() {
        if (mediaPlayer == null) {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setOnCompletionListener(this);
            mediaPlayer.setOnErrorListener(this);
            mediaPlayer.setOnInfoListener(this);
            mediaPlayer.setOnPreparedListener(this);
            mediaPlayer.setOnBufferingUpdateListener(this);
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        }
    }

    /**
     * Called when the service should be killed. This is override and called manually when the application is closed
     */
    @Override
    public void onDestroy() {
        Log.i(LOG_TAG, "Calling destroy");
    }

    /**
     * Destroys the service. See {@link RadioStreamer#onDestroy()}
     */
    public void destroy() {
        super.onDestroy();
    }

    /**
     * Plays te stream specified in the URL. MediaPlayer start is performed on
     * {@link RadioStreamer#onPrepared(android.media.MediaPlayer)}
     */
    public void playStream() {
        mediaPlayer.prepareAsync();
    }

    /**
     * Stops the stream and send a broadcast message to the main activity ({@link com.devspark.Main})
     */
    public void stopStream() {
        mediaPlayer.stop();
        sendBroadcastStreamingMessage(false);
    }

    /**
     * On error MediaPlayer listener. This is used only to log the issue.
     * 
     * @param mp
     *            the MediaPlayer object.
     * @param what
     *            the error type.
     * @param extra
     *            extra information.
     * @return always true.
     */
    public boolean onError(MediaPlayer mp, final int what, int extra) {
        StringBuilder sb = new StringBuilder();
        sb.append("Media Player Error: ");
        switch (what) {
            case MediaPlayer.MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK:
                sb.append("Not Valid for Progressive Playback");
                break;
            case MediaPlayer.MEDIA_ERROR_SERVER_DIED:
                sb.append("Server Died");
                break;
            case MediaPlayer.MEDIA_ERROR_UNKNOWN:
                sb.append("Unknown");
                break;
            default:
                sb.append(" Non standard (");
                sb.append(what);
                sb.append(")");
        }
        sb.append(" (" + what + ") ");
        sb.append(extra);
        Log.e(LOG_TAG, sb.toString());
        return true;
    }

    /**
     * On prepared MediaPlayer listener. The player will start to play the stream when this event is fired. See
     * {@link RadioStreamer#playStream}.
     * 
     * @param mp
     *            the MediaPlayer object
     */
    @Override
    public void onPrepared(final MediaPlayer mp) {
        try {
            mp.start();
            sendBroadcastStreamingMessage(true);
            obtainStreamMetaInformation();
        } catch (IllegalStateException e) {
            nonStandardError();
        }
    }

    /**
     * Obtain the title information from the stream.
     */
    private void obtainStreamMetaInformation() {
        try {
            new IcyMetadata(this).execute(STREAM_URI);
        } catch (Exception e) {
            Log.e(LOG_TAG, "Error obtaining the stream meta information");
        }
    }

    /**
     *
     */
    private void poolStreamMetaInformation(long milis) {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                obtainStreamMetaInformation();
            }
        }, milis);
    }

    /**
     * On info MediaPlayer listener. This is used only to log information.
     * 
     * @param mp
     *            the MediaPlayer object.
     * @param what
     *            the error type.
     * @param extra
     *            extra information.
     * @return always true. Needed to stop the OS calling 'onError'.
     */
    @Override
    public boolean onInfo(final MediaPlayer mp, int what, int extra) {
        Log.v(LOG_TAG, "onInfo (" + what + " - " + extra + ")");
        switch (what) {
            case 701: // MediaPlayer.MEDIA_INFO_BUFFERING_START:
                Log.i(LOG_TAG, "Start buffering...");
                break;
            case 702: // MediaPlayer.MEDIA_INFO_BUFFERING_END:
                Log.i(LOG_TAG, "End buffering...");
            case MediaPlayer.MEDIA_INFO_UNKNOWN:
                Log.i(LOG_TAG, "New metadata update ");
                obtainStreamMetaInformation();
                break;
            default:
                break;
        }
        return true;
    }

    /**
     * 
     * @param mp
     */
    @Override
    public void onCompletion(MediaPlayer mp) {
        if (mp == null)
            return;
        mp.release();
        mp = null;
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {
        if (mp == null || !mp.isPlaying())
            return;
        Log.i(LOG_TAG, "Percentage " + percent);
    }

    private final void sendBroadcastStreamingMessage(final boolean streaming) {
        final Intent intent = new Intent();
        intent.setAction(Constants.SERVICE_BROADCAST_ACTION);
        intent.addCategory(Constants.UPDATE_STATUS);
        intent.putExtra(Constants.STREAMING_FLAG, streaming);
        this.sendBroadcast(intent);
    }

    /**
     * This method is invoked when the stream metadata was obtained.
     * 
     * See {@link IcyMetadata#onPostExecute(java.util.Map)}
     * 
     * @param song
     */
    public void setSongInformation(String song) {
        final Intent intent = new Intent();
        intent.setAction(Constants.SERVICE_BROADCAST_ACTION);
        intent.addCategory(Constants.UPDATE_TITLE);
        String[] title = song.split("-");
        if (title.length > 0) {
            intent.putExtra(Constants.STREAMING_ARTIST, title[0].trim());
        }
        if (title.length > 1) {
            intent.putExtra(Constants.STREAMING_SONG, title[1].trim());
        }
        this.sendBroadcast(intent);
        poolStreamMetaInformation(2000);
    }

}