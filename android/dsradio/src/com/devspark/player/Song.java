package com.devspark.player;

/**
 * This class represents a song, its artist, the local vote and
 * the amount of likes and dislikes.
 * 
 * @author sgroh@devspark.com
 */
public class Song {
    /**
     * Separator constant.
     */
    private static String SEPARATOR = " - ";
    /**
     * The artist name.
     */
    private String artist;
    /**
     * The song name.
     */
    private String name;
    /**
     * The local vote.
     */
    private int localVote;
    /**
     * Amount of likes in server side.
     */
    private int likes;
    /**
     * Amount of dislikes in server side.
     */
    private int dislikes;

    /**
     * Creates a Song specifying the artist and the song name.
     * 
     * @param artist the artist name
     * @param name the song name
     */
    public Song(String artist, String name) {
        this.artist = artist;
        this.name = name;
    }

    /**
     * Concatenates the artist name with the song name, the separator is defined in constant SEPARATOR.
     * 
     * @return artist name contateted with song name using a string separator between them.
     */
    public String getFullName() {
        return artist + SEPARATOR + name;
    }

    /**
     * Get the artist name.
     * 
     * @return a String containing the artist name.
     */
    public String getArtist() {
        return artist;
    }

    /**
     * Set the artist name.
     * 
     * @param artist the String containing the artist name.
     */
    public void setArtist(String artist) {
        this.artist = artist;
    }

    /**
     * Set the song name.
     * 
     * @return String containing the song name.
     */
    public String getName() {
        return name;
    }

    /**
     * Set the song name.
     * 
     * @param name the String containing the song name.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Represent the user local vote.
     * 
     * @return -1 if he/she dislike the song, 0 if no local vote was made, 1 if he/she likes the song.
     */
    public int getLocalVote() {
        return localVote;
    }

    /**
     * Sets the user local vote.
     * 
     * @param localVote could be -1 if he/she dislike the song, 0 if no local vote was made, 1 if he/she likes the song.
     */
    public void setLocalVote(int localVote) {
        this.localVote = localVote;
    }

    /**
     * Returns the amount of likes in server side.
     * 
     * @return the amount of likes.
     */
    public int getLikes() {
        return likes;
    }

    /**
     * Sets the amount of likes. This should be retrieved from server side.
     * 
     * @param likes the number representing the amount of likes.
     */
    public void setLikes(int likes) {
        this.likes = likes;
    }

    /**
     * Returns the amount of dislikes in server side.
     * 
     * @return the amount of dislikes.
     */
    public int getDislikes() {
        return dislikes;
    }

    /**
     * Sets the amount of dislikes. This should be retrieved from server side.
     * 
     * @param dislikes the number representing the amount of dislikes.
     */
    public void setDislikes(int dislikes) {
        this.dislikes = dislikes;
    }
}
