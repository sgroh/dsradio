package com.devspark.player;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * This class is the songs manager maintain a simple fixed length ring.
 * Provides methods to add new songs and to know what is always the positions between 0 and MAX_LENGTH
 *
 * @author sgroh@devspark.com
 */
public class SongManager {
    /**
     * Illegal argument message.
     */
    private static String EMPTY_ARGUMENT_MSG = "The arguments must be not empty";
    /**
     * Max ring size.
     */
    private static int MAX_LENGTH = 50;
    /**
     * index to insert new elements inside the ring.
     */
    private int index;
    /**
     * Set of not repeated songs.
     */
    private Set<String> notRepeatedSongs = new HashSet<String>(MAX_LENGTH);
    /**
     * History of listened songs.
     */
    private Song[] songs = new Song[MAX_LENGTH];

    /**
     * Local instance to maintain the singleton.
     */
    private static SongManager instance = new SongManager();

    /**
     * Return the unique SongManager instance.
     *
     * @return a SongManager instance.
     */
    public static SongManager getInstance(){
        return instance;
    }

    /**
     * Default constructor.
     */
    private SongManager(){}

    /**
     * Set like as local vote in the position specified.
     *
     * @param pos  the position to be voted.
     */
    public void like(int pos){
        updateVote(pos, 1);
    }

    /**
     * Set dislike as local vote in the position specified.
     *
     * @param pos the position to be voted.
     */
    public void dislike(int pos){
        updateVote(pos, 0);
    }

    /**
     * Return where is pos inside the actual ring.
     *
     * @param pos the desired position
     * @return  the real position inside the ring.
     */
    private int realPos(int pos){
        int songAmount = notRepeatedSongs.size();
        if (songAmount < MAX_LENGTH) {
            return pos;
        } else if (pos < 0 && index ==0) { //rebase the capacity, this will always return the last element
            return songAmount -1;
        }
        return (index + songAmount -1 + pos) % MAX_LENGTH;
    }

    /**
     * Updates the local vote.
     *
     * @param pos the position being voted.
     * @param vote the vote's value.
     */
    private void updateVote(int pos, int vote){
        if (pos > MAX_LENGTH || songs[(index + pos) % MAX_LENGTH].getLocalVote()!=0){
            return;
        }
        songs[realPos(pos)].setLocalVote(vote);
    }

    /**
     * Return the position to insert and updates the index.
     *
     * @return the position to add in the ring.
     */
    private int getAvailablePosition(){
        int actuaIndex= index;
        index = (index +1) % MAX_LENGTH; //next pos after index;
        return actuaIndex;
    }

    /**
     * Adds a song to the ring.
     *
     * @param artist the artist name.
     * @param songName the song name.
     * @return null if the song exists otherwise the Song added.
     */
    public Song addSong(String artist, String songName){
        if (artist==null || songName==null || artist.length()<1 || songName.length()<1){
            throw new IllegalArgumentException(EMPTY_ARGUMENT_MSG);
        }
        String key = artist + " - " + songName;
        if (notRepeatedSongs.contains(key)) {
            return null;
        }
        notRepeatedSongs.add(key);
        int i = getAvailablePosition();
        if (songs[i]!= null) {
            notRepeatedSongs.remove(songs[i].getFullName());
        }
        Song song = new Song(artist, songName);
        songs[i] = song;
        return song;
    }

    /**
     * Gets the song in the ring.
     * @param pos the position to be query.
     * @return a {@link com.devspark.player.Song}  object.
     */
    public Song getSong(int pos){
        return songs[realPos(pos)];
    }

    /**
     * Get the amount of likes in the song that is on pos location.
     *
     * @param pos the position in the ring.
     * @return a number representing the like's amount.
     */
    public int getLikes(int pos){
        return songs[realPos(pos)].getLikes();
    }

    /**
     * Get the amount of dislikes in the song that is on pos location.
     *
     * @param pos the position in the ring.
     * @return a number representing the dislike's amount.
     */
    public int getDislikes(int pos){
        return songs[realPos(pos)].getDislikes();
    }

    /**
     * Get the local vote of the song that is on pos location.
     *
     * @param pos the position in the ring.
     * @return a number representing the local vote.
     */
    public int getLocalVote(int pos){
        return songs[realPos(pos)].getLocalVote();
    }

    /**
     * The actual song being reproduced.
     *
     * @return a {@link com.devspark.player.Song} or null if the song never was added before.
     */
    public Song reproducingNow(){
        if (notRepeatedSongs.size()==0){
            return null;
        }
        return songs[realPos(index-1)];
    }

    /**
     * Return a List with the busy ring cells.
     *
     * @return a {@link com.devspark.player.Song} list.
     */
    public List<Song> getPlayedSongs(){
        List<Song> list = new ArrayList<Song>(notRepeatedSongs.size());
        int i;
        for(i=0; i < notRepeatedSongs.size(); i++){
            list.add(songs[i]);
        }
        return list;
    }

    /**
     * The busy cells in the ring.
     *
     * @return  the amount of non repeated songs.
     */
    public int size(){
        return notRepeatedSongs.size();
    }

    /**
     * Returns the ring capacity.
     *
     * @return the max length ring capacity.
     */
    public int capacity(){
        return MAX_LENGTH;
    }


}
