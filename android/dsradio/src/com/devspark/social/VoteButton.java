package com.devspark.social;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageButton;

/**
 * Represents a vote button.
 * 
 * It's a simple Image button with an extra boolean attribute: positive.
 *
 * @author sgroh@devspark.com
 */
public class VoteButton extends ImageButton {
    /**
     * Flag to indicates if it's a positive or negative Vote.
     */
    private boolean positive;

    /**
     * Overrides the ImageButton constructor {@link android.widget.ImageButton#ImageButton(android.content.Context)}
     * @param context
     */
    public VoteButton(Context context) {
        super(context);
    }

    /**
     * Overrides the ImageButton constructor
     * {@link android.widget.ImageButton#ImageButton(android.content.Context, android.util.AttributeSet)}
     * 
     * @param context
     */
    public VoteButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Overrides the ImageButton constructor
     * {@link android.widget.ImageButton#ImageButton(android.content.Context, android.util.AttributeSet, int)}
     *
     * @param context
     */
    public VoteButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * Indicates if the button represents a positive button.
     *
     * @return true if th button represents a positive button, false otherwise.
     */
    public boolean isPositive() {
        return positive;
    }

    /**
     * Sets the flag to indicates if the button represents a positive button or a negative one.
     *
     * @param positive the flag to indicates if the button is positive or not.
     */
    public void setPositive(boolean positive) {
        this.positive = positive;
    }
}
