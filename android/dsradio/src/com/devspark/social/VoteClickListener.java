package com.devspark.social;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageButton;
import com.devspark.common.Constants;
import com.devspark.player.Song;
import com.example.dsradio.R;

/**
 * This Listener is implemented to avoid multiples click over the same song.
 *
 * @author sgroh@devspark.com
 */
public class VoteClickListener implements View.OnClickListener{
    private VoteButton oppositeButton;
    private int votedImageId;
    private Song song;
    private String category;
    private Context context;

    /**
     * Defaults constructor.
     *
     * @param votedImageId the image resource id to mark it as voted.
     * @param oppositeButton the opposite button to disable it.
     * @param song the Song that is being voted.
     * @param category the intent category that will be broadcasted.
     * @param context the context from where the intent will be broadcasted.
     */
    public VoteClickListener(final int votedImageId, final VoteButton oppositeButton, final Song song,
            final String category, final Context context) {
        this.votedImageId = votedImageId;
        this.oppositeButton = oppositeButton;
        this.song = song;
        this.category = category;
        this.context = context;
    }

    /**
     * Sets the opposite button to disable it when the click is performed.
     *
     * @param oppositeButton a {@link VoteButton}
     */
    public void setOppositeButton(final VoteButton oppositeButton){
        this.oppositeButton = oppositeButton;
    }

    /**
     * Sets the image resource id to mark it as voted.
     *
     * @param votedImageId a drawable resource id.
     */
    public void setVotedImageId(int votedImageId){
        this.votedImageId = votedImageId;
    }

    /**
     * Sets the song being voted.
     *
     * @param song a {@link com.devspark.player.Song} instance
     */
    public void setSong(final Song song){
        this.song = song;
    }

    /**
     * Sets the intent category that will be broadcasted.
     *
     * @param category a String that could be com.devspark.common.Constants.TAB_CHANGED or
     *                 com.devspark.common.Constants.UPDATE_PLAYING_SONG.
     */
    public void setCategory(final String category) {
        this.category = category;
    }

    /**
     * Sets the context from where the intent will be broadcasted.
     * @param context
     */
    public void setContext(final Context context) {
        this.context = context;
    }

    /**
     * The method to be called when some vote button is clicked.
     *
     * @param v the {@link com.devspark.social.VoteButton} instance.
     */
    @Override
    public void onClick(View v) {
        VoteButton voteButton = (VoteButton) v;
        voteButton.setEnabled(false);
        voteButton.setImageResource(votedImageId);
        if (oppositeButton!=null) {
            oppositeButton.setVisibility(View.INVISIBLE);
        }
        if (song!=null){
            if (voteButton.isPositive()){
                song.setLocalVote(1);
            } else {
                song.setLocalVote(-1);
            }
        }
        broadcastViewChanged();
        notifyServer();
    }

    /**
     * Notify the vote to the server.
     */
    private void notifyServer(){

    }

    /**
     * Broadcast that the user select another tab.
     * Vote buttons should be updated.
     */
    private void broadcastViewChanged() {
        final Intent intent = new Intent();
        intent.setAction(Constants.SERVICE_BROADCAST_ACTION);
        intent.addCategory(category);
        context.sendBroadcast(intent);
    }
}
